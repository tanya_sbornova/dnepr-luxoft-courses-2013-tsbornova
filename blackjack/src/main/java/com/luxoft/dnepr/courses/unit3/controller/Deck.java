package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

import java.util.LinkedList;
import java.util.List;

public final class Deck {

    private static final int MIN_SIZE = 1;
    private static final int MAX_SIZE = 10;

    private Deck() {
    }

    public static List<Card> createDeck(int size) {

        List<Card> listCard = new LinkedList<>();
        if (size < MIN_SIZE) {
            size = MIN_SIZE;
        }
        if (size > MAX_SIZE) {
            size = MAX_SIZE;
        }

        for (int deckN = 1; deckN <= size; deckN++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    listCard.add(new Card(rank, suit));

                }
            }
        }
        return listCard;
//		throw new UnsupportedOperationException("Not implemented yet");
    }

    public static int costOf(List<Card> hand) {

        int costAll = 0;

        if (hand != null) {
            for (Card card : hand) {
                costAll += card.getCost();
            }
        }
        return costAll;
    }
}
