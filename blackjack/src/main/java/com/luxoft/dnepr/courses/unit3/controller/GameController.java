package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class GameController {
    private static GameController controller;

    private LinkedList<Card> deck;
    private List<Card> myHand;
    private List<Card> dealersHand;

    private static final int MAX_POINTS = 21;
    private static final int DEALER_POINTS = 17;

    private GameController() {
        // инициализация переменных тут

        controller = null;
        deck = (LinkedList<Card>) Deck.createDeck(1);
        myHand = new ArrayList<>();
        dealersHand = new ArrayList<>();

    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler
     */
    void newGame(Shuffler shuffler) {

        deck = (LinkedList<Card>)  Deck.createDeck(1);
        myHand.clear();
        dealersHand.clear();

        shuffler.shuffle(deck);
        giveCards(deck, myHand, 2);
        giveCards(deck, dealersHand, 1);

    }

    private boolean giveCards(LinkedList<Card> source, List<Card> receiver, int count) {
        Card tempCard;

        if (source.size() < count) {
            return false;
        }

        for (int i = 0; i < count; i++) {
            tempCard = source.removeFirst();
            receiver.add(tempCard);
        }
        return true;
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {

        if (Deck.costOf(myHand) <= MAX_POINTS && deck.size() > 0) {
            giveCards(deck, myHand, 1);
        }
        if (Deck.costOf(myHand) <= MAX_POINTS) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {

        while (Deck.costOf(dealersHand) < DEALER_POINTS) {
            giveCards(deck, dealersHand, 1);
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {

        int myPoints = Deck.costOf(myHand);
        int dealerPoints = Deck.costOf(dealersHand);
        if (myPoints > MAX_POINTS) {
            return WinState.LOOSE;
        }
        if (myPoints < dealerPoints && dealerPoints <= MAX_POINTS) {
            return WinState.LOOSE;
        }
        if (myPoints == dealerPoints) {
            return WinState.PUSH;
        }
        return WinState.WIN;
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {

        return myHand;

    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {

        return dealersHand;
    }
}
