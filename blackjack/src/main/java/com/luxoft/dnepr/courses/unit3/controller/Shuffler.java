package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;

import java.util.List;

interface Shuffler {
    void shuffle(List<Card> deck);
}
