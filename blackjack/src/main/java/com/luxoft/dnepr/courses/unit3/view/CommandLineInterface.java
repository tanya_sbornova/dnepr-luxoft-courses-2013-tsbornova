package com.luxoft.dnepr.courses.unit3.view;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

public class CommandLineInterface {

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: Tanya Sbornova)\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();

        controller.newGame();

        output.println();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    private boolean execute(String command, GameController controller) {

        switch (command) {
            case Command.EXIT:
                return false;
            case Command.HELP:
                commandHelp();
                return true;
            case Command.MORE:
                if (commandMore(controller)) {
                    return true;
                } else {
                    return false;
                }
            case Command.STOP:
                commandStop(controller);
                return false;
            default:
                output.println("Invalid command");
                return true;
        }
    }

    private boolean commandMore(GameController controller) {
        controller.requestMore();
        printState(controller);
        if (controller.getWinState() == WinState.LOOSE) {
            finalMessage(controller.getWinState());
            return false;
        }
        return true;
    }

    private void commandStop(GameController controller) {
        controller.requestStop();
        output.println("Dealer turn:");
        output.println();
        printState(controller);
        finalMessage(controller.getWinState());
    }

    private void commandHelp() {

        output.println("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game");
    }

    private void finalMessage(WinState winState) {

        output.println();
        switch (winState) {
            case LOOSE:
                output.println("Sorry, today is not your day. You loose.");
                break;
            case WIN:
                output.println("Congrats! You win!");
                break;
            case PUSH:
                output.println("Push. Everybody has equal amount of points.");
                break;
        }
    }

    private void format(List<Card> list) {

        for (Card card : list) {
            output.print(card.getRank().getShotRank() + " ");
        }
        output.print("(total " + Deck.costOf(list) + ")\n");
    }

    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();

        format(myHand);

        List<Card> dealersHand = controller.getDealersHand();

        format(dealersHand);
    }
}
