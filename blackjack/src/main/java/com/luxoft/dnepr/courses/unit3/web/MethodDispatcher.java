package com.luxoft.dnepr.courses.unit3.web;

import java.util.List;

import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class MethodDispatcher {

	/**
	 * 
	 * @param request
	 * @param response
	 * @return response or <code>null</code> if wasn't able to find a method.
	 */
	public Response dispatch(Request request, Response response) {
		String method = request.getParameters().get("method");
		if (method == null) {
			return null;
		}
        switch (method) {
            case ("newGame"):
                return newGame(response);
            case ("requestMore"):
                return requestMore(response);
            case ("requestStop"):
                return  requestStop(response);
            case ("winState"):
                return  winState(response);
            case ("exit"):
                break;
		}
		return null;
	}

    private Response winState(Response response) {

        WinState winState = GameController.getInstance().getWinState();
        response.write("{\"result\": {\"winstate\": \"");
        response.write(winState.toString());
        response.write("\"}}");
        return response;
    }

    private Response newGame(Response response) {

        GameController.getInstance().newGame();
        response.write("{\"result\": ");
        response.write("{\"myhand\": ");
        writeHand(response, GameController.getInstance().getMyHand(), "newGame");

        response.write(",\"dealershand\": ");
        writeHand(response, GameController.getInstance().getDealersHand(), "newGame");
        response.write("}");
        response.write("}");

        return response;
    }

	private Response requestMore(Response response) {

        GameController.getInstance().requestMore();
		response.write("{\"result\": ");
		response.write("{\"myhand\": ");
		
		writeHand(response, GameController.getInstance().getMyHand(), "requestMore");
		
		response.write("}");
        response.write("}");
		return response;
	}

    private Response requestStop(Response response) {

        GameController.getInstance().requestStop();
        response.write("{\"result\": ");
        response.write("{\"dealershand\": ");

        writeHand(response, GameController.getInstance().getDealersHand(), "requestStop");

        response.write("}");
        response.write("}");
        return response;
    }

	private void writeHand(Response response, List<Card> hand, String method) {
		boolean isFirst = true;

        response.write("[");
        if (method.equals("requestMore")) {
            Card card = hand.get(hand.size()-1);
            writeCard(response, card);
            response.write("]");
            return;
        }
		for (Card card : hand) {
			if (isFirst) {
                if (method.equals("requestStop")) {
                    method = "";
                    continue;
                }
				isFirst = false;
			} else {
				response.write(",");
			}
            writeCard(response, card);
		}
		response.write("]");
	}

    private void writeCard(Response response, Card card){

        response.write("{\"rank\": \"");
        response.write(card.getRank().getName());
        response.write("\", \"suit\": \"");
        response.write(card.getSuit().name());
        response.write("\"");
        response.write("}");
    }
}
