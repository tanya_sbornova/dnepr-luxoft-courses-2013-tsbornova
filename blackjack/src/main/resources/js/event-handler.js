function generationHTML(hand, array) {
	var suit, suit_red, suit_black, card2;
	
	if (hand === 'dealershand') {
		var card1 = 'card_dealer';
		container_allign = '.container_allign:eq(3)';
	}
	else {
		var card1 = 'card_player';
		container_allign = '.container_allign:eq(4)'
	}
	for (var i = 0; i < array.length; i++) {
		$(container_allign).append('<div class="' + card1 + '"></div>');	
		suit = array[i].suit.toLowerCase();		
		card2 = '.' + card1 + ':last';	
		$(card2).append('<div class="' + suit + '"></div>');
		
		$(card2).append('<div class="rank">' + array[i].rank + '</div>');			
		rank = container_allign + ' .rank:last';	
		if (suit === 'diamonds' || suit === 'hearts') {
			$(rank).css('color','#CD0000');
		} else {
			$(rank).css('color','#000000');
		} 		
	}       
};
function buttonclose() {
	PopUpHide();
	$("div.card_dealer").remove();	
	$("div.card_player").remove();	
	$("div.button_close").empty();	
	pressStart();	
}
function addButton() { 	
	var btnclose = $('<input type="button" name="Close" value="x" onclick="buttonclose()">');	
    btnclose.appendTo($('.button_close')); 
}
function checkwinstate(command) {	
	$.ajax({
		type: "GET",
		url: "http://localhost:8081/service",
		data: "method=winState"
	}).done(function(response) {
		var array = jQuery.parseJSON(response);			   
		var rez = array.result.winstate;
		if (command != 'stand' && rez !== 'LOOSE')  {
			return;
		}  		
		switch (rez){
		case 'WIN':
			$('#message').text('Congrats! You win!');					
			addButton();			
			PopUpShow();			
			break;		
		case 'LOOSE':
			$('#message').text('Sorry, today is not your day. You loose');			
			addButton();
			PopUpShow();			
			break;
		case 'PUSH':
			$('#message').text('Push. Everybody has equal amount of points');			
			addButton();
			PopUpShow();							
		}	
	});
}
function pressStart() {
	$("input[name='Start']").css('background','#DAA520');
	$("input[name='Hit']").css('background','#8B6914');
	$("input[name='Stand']").css('background','#8B6914');
	$("input[name='Hit']").unbind();
	$("input[name='Stand']").unbind();	
	$("input[name='Start']").bind("click", function() {		
		$("input[name='Start']").unbind();
		$("input[name='Start']").css('background','#8B6914');
		pressHit();
		pressStand();
		$.ajax({
			type: "GET",
			url: "http://localhost:8081/service",
			data: "method=newGame"
		}).done(function(response) {						
			var arrcards = jQuery.parseJSON(response);				
			generationHTML('dealershand', arrcards.result.dealershand);
			generationHTML('myhand',arrcards.result.myhand);			
		});
    });
}
function pressHit() {
	$("input[name='Hit']").css('background','#DAA520');
	$("input[name='Hit']").bind("click", function() {		
		$.ajax({
			type: "GET",
			url: "http://localhost:8081/service",
			data: "method=requestMore"
		}).done(function(response) {
			var arrcards = jQuery.parseJSON(response);				
			generationHTML('myhand',arrcards.result.myhand);			
			checkwinstate('hit');
		});
    });
}
function pressStand() {
	$("input[name='Stand']").css('background','#DAA520');	
	$("input[name='Stand']").bind("click", function() {		
		$.ajax({
			type: "GET",
			url: "http://localhost:8081/service",
			data: "method=requestStop"
		}).done(function(response) {			
			var arrcards = jQuery.parseJSON(response);								
			generationHTML('dealershand', arrcards.result.dealershand);
			checkwinstate('stand');
		});
    });
}
function PopUpHide(){
    $(".wind_popup").hide();	
}
function PopUpShow(){
    $(".wind_popup").show();	
}
$(document).ready(function(){     
	PopUpHide();
	pressStart();
});