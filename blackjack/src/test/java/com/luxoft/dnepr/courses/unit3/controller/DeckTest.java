package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DeckTest {

	@Test
	public void testCreate() {
		assertEquals(52, Deck.createDeck(1).size());
		assertEquals(104, Deck.createDeck(2).size());
		assertEquals(208, Deck.createDeck(4).size());
		assertEquals(52 * 10, Deck.createDeck(10).size());
		
		assertEquals(52 * 10, Deck.createDeck(11).size());
		assertEquals(52, Deck.createDeck(-1).size());
		
		List<Card> deck = Deck.createDeck(2);
		int i = 0;
		
		for (int deckN = 0; deckN < 2; deckN++) {
			for (Suit suit : Suit.values()) {
				for (Rank rank : Rank.values()) {
					// расскоментировать когда будут готовы п1-п3
					assertEquals(suit, deck.get(i).getSuit());
					assertEquals(rank, deck.get(i).getRank());
					assertEquals(rank.getCost(), deck.get(i).getCost());
					i++;
				}
			}
		}
	}
	
	@Test
	public void testCostOf() {
        List<Card> testHand = new ArrayList<>();
        testHand.add(new Card(Rank.RANK_5, Suit.SPADES));
        testHand.add(new Card(Rank.RANK_ACE, Suit.DIAMONDS));
        List<Card> testHand2 = new ArrayList<>();
        testHand2.add(new Card(Rank.RANK_2, Suit.SPADES));
        testHand2.add(new Card(Rank.RANK_10, Suit.SPADES));
        testHand2.add(new Card(Rank.RANK_JACK, Suit.DIAMONDS));
        List<Card> testHand3 = new ArrayList<>();
        testHand3.add(new Card(Rank.RANK_ACE, Suit.CLUBS));
        List<Card> testHand4 = new ArrayList<>();

        assertEquals(16, Deck.costOf(testHand));
        assertEquals(22, Deck.costOf(testHand2));
        assertEquals(11, Deck.costOf(testHand3));
        assertEquals(0, Deck.costOf(testHand4));

	}
}
