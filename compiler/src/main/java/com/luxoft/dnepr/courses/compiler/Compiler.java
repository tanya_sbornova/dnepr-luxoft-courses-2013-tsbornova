package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compiler {

    public static final boolean DEBUG = true;

    private static final String EXPRESSION_PATTERN = "[-()*/+]";
    private static final String[] PRIORITY_OPERATIONS = new String[]{"+", "-", "*", "/"};

    private static List<Bracket> listBrackets;
    private static List<String> expression;

    public static void main(String[] args) {

        byte[] byteCode = compile(getInputString(args));

        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {

        ByteArrayOutputStream result = new ByteArrayOutputStream();

        Expression currentExpression = tokenString(input);

        if (currentExpression.getEndIndex() < 1) {
            throw new CompilationException("Error expression");
        }
        parseBrackets(expression);

        trimBrackets(currentExpression);

        action(currentExpression, result);
        putStack("Print", result, 0d);

        return result.toByteArray();
    }

    private static Expression tokenString(String stringToken) {

        int indStart = 0, indEnd;
        String tempString;

        expression = new ArrayList<>();

        Pattern p = Pattern.compile(EXPRESSION_PATTERN);
        Matcher m;
        try {
            m = p.matcher(stringToken);
        } catch (NullPointerException e) {
            throw new CompilationException("Error expression");
        }

        while (m.find()) {
            indEnd = m.start() >= indStart ? m.start() : indStart;
            tempString = stringToken.substring(indStart, indEnd).trim();
            if (tempString.length() > 0) {
                expression.add(tempString);
            }
            expression.add(m.group());
            indStart = indEnd + 1;
        }
        tempString = stringToken.substring(indStart, stringToken.length()).trim();
        if (tempString.length() > 0) {
            expression.add(tempString);
        }
        return new Expression(0, expression.size() - 1);
    }

    private static void parseBrackets(List<String> expression) {

        listBrackets = new LinkedList<>();
        LinkedList<Bracket> tempListBrackets = new LinkedList<>();
        Bracket tempBracket;

        int elemIndex = 0;
        for (String element : expression) {

            if (element.equals("(")) {
                tempListBrackets.add(new Bracket(elemIndex, 0));
            }  else
            if (element.equals(")")) {
                try {
                    tempBracket = tempListBrackets.pollLast();
                    tempBracket.setEndIndex(elemIndex);
                    listBrackets.add(tempBracket);
                } catch (NullPointerException e) {
                    throw new CompilationException("Error brackets");
                }
            }
            elemIndex++;
        }
        if (!tempListBrackets.isEmpty()) {
            throw new CompilationException("Error brackets");
        }
    }

    private static void action(Expression currentExpression, ByteArrayOutputStream result) {

        int tempIndex;
        Expression newExpression;

        for (int i = 0; i < 4; i++) {
            tempIndex = searchSign(currentExpression, PRIORITY_OPERATIONS[i]);
            if (tempIndex >= 0) {
                newExpression = cutExpression(currentExpression.getStartIndex(), tempIndex - 1);
                if (isNumber(newExpression)) {
                    putStack("Push", result, Double.parseDouble(expression.get(newExpression.getStartIndex())));
                } else {
                    action(newExpression, result);
                }
                newExpression = cutExpression(tempIndex + 1, currentExpression.getEndIndex());
                if (isNumber(newExpression)) {
                    putStack("Push", result, Double.parseDouble(expression.get(newExpression.getStartIndex())));
                } else {
                    action(newExpression, result);
                }
                putStack(PRIORITY_OPERATIONS[i], result, 0d);
                break;
            } else if (i == 3) {
                throw new CompilationException("Error expression");
            }
        }
    }

    private static int searchSign(Expression currentExpression, String operation) {

        int index = currentExpression.getEndIndex();

        ListIterator iter = expression.listIterator(index);
        String tempElem = (String) iter.next();
        while (iter.hasPrevious() && index >= currentExpression.getStartIndex()) {
            if (tempElem.equals(operation) && !isIntoBrackets(index)) {
                return index;
            }
            index = iter.previousIndex();
            tempElem = (String) iter.previous();

        }
        return -1;
    }

    private static boolean isIntoBrackets(int index) {

        for (Bracket bracket : listBrackets) {
            if (index > bracket.getStartIndex() && index < bracket.getEndIndex()) {
                return true;
            }
        }
        return false;
    }

    private static void trimBrackets(Expression expression) {

        Bracket tempBracket;
        int startIndex = expression.getStartIndex();
        int endIndex = expression.getEndIndex();
        ListIterator iter = listBrackets.listIterator(listBrackets.size());
        while (iter.hasPrevious()) {
            tempBracket = (Bracket) iter.previous();
            if (startIndex == tempBracket.getStartIndex() && endIndex == tempBracket.getEndIndex()) {
                iter.remove();
                expression.setStartIndex(++startIndex);
                expression.setEndIndex(--endIndex);
            }
        }
    }

    private static Expression cutExpression(int startIndex, int endIndex) {

        Expression newExpression = new Expression(startIndex, endIndex);
        trimBrackets(newExpression);
        return newExpression;
    }

    private static boolean isNumber(Expression currentExpression) {

        if (currentExpression.getStartIndex() != currentExpression.getEndIndex()) {
            return false;
        }
        try {
            Double.parseDouble(expression.get(currentExpression.getStartIndex()));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static void putStack(String command, ByteArrayOutputStream result, Double value) {

        switch (command) {
            case "Push":
                addCommand(result, VirtualMachine.PUSH, value);
                break;
            case "+":
                addCommand(result, VirtualMachine.ADD);
                break;
            case "-":
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.SUB);
                break;
            case "*":
                addCommand(result, VirtualMachine.MUL);
                break;
            case "/":
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.DIV);
                break;
            case "Print":
                addCommand(result, VirtualMachine.PRINT);
        }
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result //     * @param command
     */

    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     * <p/>
     * //     * @param result
     * //     * @param command
     * //     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
