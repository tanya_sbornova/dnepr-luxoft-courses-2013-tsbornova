package com.luxoft.dnepr.courses.compiler;

public class Expression {

    private int startIndex;
    private int endIndex;

    public Expression(int startIndex, int endIndex) {

        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }
}

