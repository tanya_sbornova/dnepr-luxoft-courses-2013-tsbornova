package com.luxoft.dnepr.courses.compiler;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class CompilerTest extends AbstractCompilerTest {

    @Test
    public void testErrors() {
        try {
            assertCompiled(0, "  100a / 2 / 5 / 10");
            fail("Should have thrown an CompilationException with message Error expression");
        } catch (CompilationException e) {
            assertThat(e.getMessage(), containsString("Error expression"));
        }
        try {
            assertCompiled(0, " (100 / 2 ))");
            fail("Should have thrown an CompilationException with message Error brackets");
        } catch (CompilationException e) {
            assertThat(e.getMessage(), containsString("Error brackets"));
        }
        try {
            assertCompiled(0, " ((100 - 2 )");
            fail("Should have thrown an CompilationException with message Error brackets");
        } catch (CompilationException e) {
            assertThat(e.getMessage(), containsString("Error brackets"));
        }
        try {
            assertCompiled(0, " 100 - + 2 ");
            fail("Should have thrown an CompilationException with message Error expression");
        } catch (CompilationException e) {
            assertThat(e.getMessage(), containsString("Error expression"));
        }
        try {
            assertCompiled(0, " (100 - 2 * )");
            fail("Should have thrown an CompilationException with message Error expression");
        } catch (CompilationException e) {
            assertThat(e.getMessage(), containsString("Error expression"));
        }
    }

    @Test(expected = CompilationException.class)
    public void testErrors2() {

        assertCompiled(0, "  (8/(2 + 2 ))/2 * 3)");
        assertCompiled(4, "2+2 .");
        assertCompiled(5, " 2 + 3 )");
        assertCompiled(0, "+");
        assertCompiled(0, " - 8");
        assertCompiled(0, "50 *");
        assertCompiled(0, "");
        assertCompiled(0, null);
        assertCompiled(0, "6 ++");
        assertCompiled(0, " ()");
        assertCompiled(0, " ) 7+ 8 (");
        assertCompiled(0, " ( )( )");
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testErrors3() {
        exception.expect(CompilationException.class);
        exception.expectMessage(containsString("Error expression"));
        assertCompiled(0, " ");
    }

    @Test
    public void testErrors4() {
        exception.expect(CompilationException.class);
        exception.expectMessage(containsString("Error brackets"));
        assertCompiled(0, " (((");
    }

    @Test
    public void testSimple() {
        assertCompiled(4, "2+2");
        assertCompiled(5, " 2 + 3 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(6, " 2 * 3 ");
        assertCompiled(4.5, "  9 /2 ");
        assertCompiled(426.16, "15.65+ 410.51");
        assertCompiled(50.8625, "15.65 * 3.25");
        assertCompiled(-12.39, " 3.26 -15.65");
    }

    @Test
    public void testComplex() {
        assertCompiled(12, "  (2 + 2 ) * 3 ");
        assertCompiled(8.5, "  2.5 + 2 * 3 ");
        assertCompiled(8.5, " (((2 *3 + 2.5)))");
        assertCompiled(1, " (2+2)/(2+2)");
        assertCompiled(0, "(2+2)-(2+2)");
        assertCompiled(1, "  100. / 2. / 5. / 10");
        assertCompiled(12, "(((2 + 2 )) * (3))");
        assertCompiled(79.66666666666667, "80 - (1 * (1.5 + 0.5)/6)");
        assertCompiled(3, "  (8/(2 + 2 ))/2 * 3");
        assertCompiled(4, " 2*2/2*2/2*2/2*2");
        assertCompiled(0.333333, "(8/(2+2))/(2*3)");
        assertCompiled(0.7, "  .5 + .2");
        assertCompiled(-6.7, " ( .1 + .2 ) - (3+4)");
        assertCompiled(3, "  (8/(2 + 2 ))/2 * 3");
        assertCompiled(5, "   6 * (7 - 2 ) / ((2 + 4))");
        assertCompiled(12, "   6 * ((7 + 5 ) / (2 + 4))");

    }
}
