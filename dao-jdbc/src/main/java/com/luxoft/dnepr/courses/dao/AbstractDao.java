package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.exception.UserNotFound;
import com.luxoft.dnepr.courses.model.Employee;
import com.luxoft.dnepr.courses.model.Entity;
import com.luxoft.dnepr.courses.model.Redis;
import com.luxoft.dnepr.courses.storage.InitDB;

import java.io.IOException;
import java.sql.*;

public abstract class AbstractDao<E extends Entity> implements IDao<E> {

    private final static int STARTING_ID = 0;
    private final static int COLUMN_ID = 1;
    private final static int COLUMN_VALUE = 2;
    private final static int NULL_PARAMETER = -1;
    private final static String TYPE_DAO_EMPLOYEE = "dao_schema.employee";
    private final static String TYPE_DAO_REDIS = "dao_schema.redis";
    private String typeDao;
    private String typeDaoColumn;
    private int parameter;
    private Connection conn;


    public AbstractDao(Class<E> type) throws ClassNotFoundException, SQLException, IOException {

        if (type == Employee.class) {
            typeDao = TYPE_DAO_EMPLOYEE;
        } else {
            typeDao = TYPE_DAO_REDIS;
        }
    }
    private void closeResource(PreparedStatement pStmt, Connection conn) throws SQLException  {
        conn.commit();
        conn.close();
        pStmt.close();
    }

    private void setParameter(E entity) {
        if (entity.getClass() == Employee.class) {
            parameter = ((Employee) entity).getSalary();
            typeDaoColumn = "salary";
        } else {
            parameter = ((Redis) entity).getWeight();
            typeDaoColumn = "weight";
        }
    }

    private Integer execQuery(String query, int id, int numberColumn) throws SQLException {
        conn = InitDB.getConnect();
        PreparedStatement pStmt = conn.prepareStatement(query);
        if (id != NULL_PARAMETER) {
            pStmt.setInt(COLUMN_ID, id);
        }
        ResultSet result = pStmt.executeQuery();
        Integer value = null;
        while (result.next()) {
            value = result.getInt(numberColumn);
        }
        closeResource(pStmt, conn);
        return value;
    }

    private int execUpdate(String query, int id, int value) throws SQLException {
        conn = InitDB.getConnect();
        PreparedStatement pStmt = conn.prepareStatement(query);
        pStmt.setInt(COLUMN_ID, id);
        if (value != NULL_PARAMETER ) {
            pStmt.setInt(COLUMN_VALUE, value);
        }
        int result = pStmt.executeUpdate();
        conn.commit();
        conn.close();
        pStmt.close();
        return result;
    }

    private Long nextId() throws SQLException {

        Integer maxId = execQuery("SELECT MAX(ID) FROM " + typeDao, NULL_PARAMETER, COLUMN_ID);
        if (maxId == null) {
            maxId = STARTING_ID;
        }
        return Long.parseLong(maxId.toString());
    }

    private boolean isIdExist(long id) throws SQLException {

        Integer resultId = execQuery("SELECT ID FROM " + typeDao + " WHERE ID = ? ", (int) id, COLUMN_ID);
        return (resultId != null);
    }

    public E save(E entity) throws SQLException {
        Long id;

        if (entity == null) {
            return null;
        }
        id = entity.getId();
        if (!(id == null) && isIdExist(id)) {
            throw new UserAlreadyExist("User " + id + " already exist");
        }
        if (id == null) {
            id = nextId() + 1;
            entity.setId(id);
        }
        setParameter(entity);
        execUpdate("INSERT INTO " + typeDao + " VALUES(?, ?)", id.intValue(), parameter);
        return entity;
    }

    public E update(E entity) throws SQLException {
        Long id;

        if (entity == null) {
            return null;
        }
        id = entity.getId();
        if (id == null || !isIdExist(id)) {
            throw new UserNotFound("User " + id + " not found");
        }
        setParameter(entity);
        execUpdate("UPDATE " + typeDao + " SET " + typeDaoColumn + " = " + parameter + " WHERE ID = ?", id.intValue(), NULL_PARAMETER);
        return entity;
    }

    public E get(long id) throws SQLException {

        Integer value = execQuery("SELECT * FROM " + typeDao + " WHERE ID = ? ", (int) id, COLUMN_VALUE);
        if (value == null) {
            return null;
        }
        if (typeDao.equals(TYPE_DAO_EMPLOYEE)) {
            Employee entity = new Employee();
            entity.setId(id);
            entity.setSalary(value);
            return (E) entity;
        } else {
            Redis entity = new Redis();
            entity.setId(id);
            entity.setWeight(value);
            return (E) entity;
        }
    }

    public boolean delete(long id) throws SQLException {

        int result = execUpdate("DELETE FROM " + typeDao + " WHERE ID = ?", (int) id, NULL_PARAMETER);
        return result > 0;
    }

}
