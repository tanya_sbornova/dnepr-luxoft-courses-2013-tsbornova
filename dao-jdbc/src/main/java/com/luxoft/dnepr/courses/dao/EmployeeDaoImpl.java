package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.model.Employee;

import java.io.IOException;
import java.sql.SQLException;


public class EmployeeDaoImpl extends AbstractDao<Employee> {

    public EmployeeDaoImpl()throws ClassNotFoundException, SQLException, IOException {

        super(Employee.class);
    }


}
