package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.model.Entity;

import java.sql.SQLException;

public interface IDao<E extends Entity> {

    E save(E e) throws SQLException;

    E update(E e) throws  SQLException;

    E get(long id) throws  SQLException;

    boolean delete(long id) throws  SQLException;

}
