package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.model.Redis;

import java.io.IOException;
import java.sql.SQLException;

public class RedisDaoImpl extends AbstractDao<Redis> {

    public RedisDaoImpl() throws  ClassNotFoundException, SQLException, IOException {

        super(Redis.class);
    }
}
