package com.luxoft.dnepr.courses.exception;

public class UserNotFound extends RuntimeException {

    public UserNotFound (String message) {

        super(message);

    }
}
