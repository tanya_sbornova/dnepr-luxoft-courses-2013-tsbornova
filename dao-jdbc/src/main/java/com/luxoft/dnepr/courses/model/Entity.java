package com.luxoft.dnepr.courses.model;

public class Entity {

    private Long id;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
}
