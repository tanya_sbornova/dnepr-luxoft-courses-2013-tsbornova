package com.luxoft.dnepr.courses.model;

public class Redis  extends Entity {

    private int weight;

    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }
}
