package com.luxoft.dnepr.courses.storage;

import org.h2.jdbcx.JdbcConnectionPool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InitDB {
    private final static String CONNECTION_STRING = "jdbc:h2:~/test";
    private final static String USER = "sa";
    private final static String PASSWORD = "";
    private final static String DRIVER_DB = "org.h2.Driver";
    private static JdbcConnectionPool connPool = JdbcConnectionPool.create(CONNECTION_STRING, USER, PASSWORD);

    public static void driverRegister() throws ClassNotFoundException, IOException, SQLException, InterruptedException {

        Class.forName(DRIVER_DB);
    }

    public static void createObjects() throws ClassNotFoundException, SQLException, IOException {

        Connection conn = getConnect();
        PreparedStatement pStmt = conn.prepareStatement("CREATE SCHEMA IF NOT EXISTS dao_schema AUTHORIZATION SA");
        pStmt.executeUpdate();
        String[] strCreate = {
                "DROP TABLE IF EXISTS dao_schema.employee",
                "DROP TABLE IF EXISTS dao_schema.redis",
                "CREATE TABLE IF NOT EXISTS dao_schema.employee(id INTEGER PRIMARY KEY, salary INTEGER)",
                "CREATE TABLE IF NOT EXISTS dao_schema.redis(id INTEGER PRIMARY KEY, weight INTEGER)",
                };
        for (String str : strCreate) {
            pStmt = conn.prepareStatement(str);
            pStmt.executeUpdate();
        }
        pStmt.close();
        conn.close();
    }

    public static Connection getConnect() throws SQLException {

        return connPool.getConnection();
    }

    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException, InterruptedException {

        try {
            driverRegister();
        } catch (ClassNotFoundException exception) {
            exception.printStackTrace();
        }
        createObjects();
    }
}
