package com.luxoft.dnepr.courses;
import com.luxoft.dnepr.courses.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.dao.IDao;
import com.luxoft.dnepr.courses.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.exception.UserNotFound;
import com.luxoft.dnepr.courses.model.Employee;
import com.luxoft.dnepr.courses.storage.InitDB;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EtalonTest {

    IDao<Employee> dao;
    @Before
    public void init() throws ClassNotFoundException, SQLException, IOException {
        dao = new EmployeeDaoImpl();
        InitDB.createObjects();
        dao.save(createEmployee(1L, 1));
        dao.save(createEmployee(2L, 2));
        dao.save(createEmployee(3L, 3));
        dao.save(createEmployee(4L, 4));
        dao.save(createEmployee(5L, 5));
    }

    @Test
    public void saveBasicTest() throws UserAlreadyExist, SQLException {
        Employee emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(6), emp.getId());

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(7), emp.getId());

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(8), emp.getId());
    }

    @Test
    public void saveComplicatedTest() throws UserAlreadyExist, SQLException {
        Employee emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(6), emp.getId());

        dao.delete(1L);

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(7), emp.getId());

        dao.delete(2L);
        dao.delete(3L);

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(8), emp.getId());

        dao.delete(7L);
        dao.delete(8L);

        emp = createEmployee(null, 1);
        emp = dao.save(emp);
        Assert.assertEquals(new Long(7), emp.getId());
    }

    @Test(expected = UserAlreadyExist.class)
    public void saveAlreadyExistsTest() throws UserAlreadyExist, SQLException {
        Employee emp = createEmployee(1L, 1);
        dao.save(emp);
    }

    @Test(expected = UserAlreadyExist.class)
    public void saveAlreadyExists2Test() throws UserAlreadyExist, SQLException {
        Employee emp = createEmployee(5L, 1);
        dao.save(emp);
    }

    @Test
    public void updateOkTest() throws UserNotFound, SQLException {
        Employee emp = createEmployee(1L, 10);
        emp = dao.update(emp);
        Assert.assertEquals(new Long(1), emp.getId());
        Assert.assertEquals(10, emp.getSalary());
        emp = (Employee) dao.get(1L);
        Assert.assertEquals(new Long(1), emp.getId());
        Assert.assertEquals(10, emp.getSalary());
    }

    @Test(expected = UserNotFound.class)
    public void updateFail1Test() throws UserNotFound, SQLException {
        Employee emp = createEmployee(null, 10);
        emp = dao.update(emp);
    }

    @Test(expected = UserNotFound.class)
    public void updateFail2Test() throws UserNotFound, SQLException {
        Employee emp = createEmployee(6L, 10);
        emp = dao.update(emp);
    }

    @Test
    public void getTest() throws  SQLException {
        Employee emp = dao.get(3);
        Assert.assertEquals(new Long(3), emp.getId());
        Assert.assertEquals(3, emp.getSalary());

        emp = dao.get(10);
        Assert.assertNull(emp);
    }

    @Test
    public void deleteTest() throws SQLException {
        Assert.assertFalse(dao.delete(7L));
        Assert.assertFalse(dao.delete(8L));
        Assert.assertFalse(dao.delete(9L));

        Assert.assertTrue(dao.delete(1L));
        Assert.assertTrue(dao.delete(2L));
        Assert.assertTrue(dao.delete(3L));
        Assert.assertTrue(dao.delete(4L));
        Assert.assertTrue(dao.delete(5L));

        Assert.assertFalse(dao.delete(1L));
        Assert.assertFalse(dao.delete(2L));
    }

    private static Employee createEmployee(Long id, int weight) {
        Employee emp = new Employee();
        emp.setId(id);
        emp.setSalary(weight);
        return emp;
    }
}
