package com.luxoft.courses.javaWeb;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Dummy extends HttpServlet {

    private static ConcurrentMap<String, String> map = new ConcurrentHashMap<>();
    Gson gson = new Gson();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String paramName = request.getParameter("name");
        String paramAge = request.getParameter("age");
        PrintWriter writer = response.getWriter();
        if (isParametersNull(response, writer, paramName, paramAge)) {
            return;
        }
        String storedValue = map.putIfAbsent(paramName, paramAge);
        if (storedValue == null) {
            formResponse(response, writer, HttpServletResponse.SC_CREATED, "");
        } else {
            formResponse(response, writer, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Name " + paramName + " already exists");
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String paramName = request.getParameter("name");
        String paramAge = request.getParameter("age");
        PrintWriter writer = response.getWriter();
        if (isParametersNull(response, writer, paramName, paramAge)) {
            return;
        }
        String storedValue = map.replace(paramName, paramAge);
        if (storedValue != null) {
            formResponse(response, writer, HttpServletResponse.SC_ACCEPTED, "");
        } else {
            formResponse(response, writer, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Name " + paramName + " does not exist");
        }
    }

    private void formResponse(HttpServletResponse response, PrintWriter writer, int status, String message) {

        response.setStatus(status);
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        String errorMessage = gson.toJson(Error.getInstance(message));
        if (status == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {
            writer.println(errorMessage);
        }
    }

    private boolean isParametersNull(HttpServletResponse response, PrintWriter writer, String name, String age) {

        if (name == null || age == null) {
            formResponse(response, writer, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Illegal parameters");
            return true;
        }
        return false;
    }

    private static class Error {

        String error;

        private Error(String error) {
            this.error = error;
        }

        public static Error getInstance(String message) {

            return new Error(message);
        }
    }
}
