package com.luxoft.courses.javaWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class Login extends HttpServlet {
    static Map<String, String> map = ParserXML.getMap();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.getWriter().write("login");
        response.sendRedirect("index.html");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html; charset=utf-8");
        HttpSession session = request.getSession();
        session.invalidate();
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        response.getWriter().write(userName);
        response.getWriter().write(password);
        if (userName == null || password == null) {
            response.sendRedirect("index.html");
        }
        if (isUserExist(userName, password)) {
            request.getSession().setAttribute("userName", request.getParameter("userName"));
            request.getRequestDispatcher("user").forward(request, response);
        } else {
            request.setAttribute("error", "Wrong login or password");
            request.setAttribute("userName", userName);
            request.setAttribute("password", password);
            request.getRequestDispatcher("index.html").forward(request, response);
        }
    }

    public static boolean isUserExist(String userName, String password) {

        String passwordInMap = map.get(userName);
        return (passwordInMap != null && passwordInMap.equals(password)) ? true : false;
    }
}
