<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="com.luxoft.courses.javaWeb.*"%>
<% if (request.getSession(false).getAttribute("userName")!=null) {
      String userName = (String) request.getSession().getAttribute("userName");
      String password = request.getParameter("password");
      if (!Login.isUserExist(userName, password))  {
          response.sendRedirect("index.html");
      }
   } else {
       response.sendRedirect("index.html");
   } %>
<html>
<head>
    <title>User login</title>
    <meta charset="UTF-8"/>
    <link href="${pageContext.request.contextPath}/styles/layout.css" rel="stylesheet">
</head>

<body>
<a href="index.html" >logout</a>
<div id="welcome">
    <h2> Hello ${userName}! </h2>
</div>
</body>
</html>