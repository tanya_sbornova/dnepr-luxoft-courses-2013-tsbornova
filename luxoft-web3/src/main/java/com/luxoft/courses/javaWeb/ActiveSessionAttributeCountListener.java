package com.luxoft.courses.javaWeb;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import java.util.concurrent.atomic.AtomicLong;

public class ActiveSessionAttributeCountListener implements HttpSessionAttributeListener {

    @Override
    public void attributeAdded(HttpSessionBindingEvent httpSessionBindingEvent) {
        String role = (String) httpSessionBindingEvent.getSession().getAttribute("role");
        switch (role) {
            case "user":
                getActiveSessionsUser(httpSessionBindingEvent).getAndIncrement();
                break;
            case "admin":
                getActiveSessionsAdmin(httpSessionBindingEvent).getAndIncrement();
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent httpSessionBindingEvent) {

        String role = (String) httpSessionBindingEvent.getSession().getAttribute("role");
        if (role.equals("user")) {
            getActiveSessionsUser(httpSessionBindingEvent).getAndDecrement();
        } else {
            getActiveSessionsAdmin(httpSessionBindingEvent).getAndDecrement();
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {
    }

    private AtomicLong getActiveSessionsUser(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_USER);
    }

    private AtomicLong getActiveSessionsAdmin(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ADMIN);
    }
}
