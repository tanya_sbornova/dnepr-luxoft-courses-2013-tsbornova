package com.luxoft.courses.javaWeb;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicLong;

public class ActiveSessionCountListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndIncrement();
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndDecrement();
        String role = (String) hse.getSession().getAttribute("role");
        if (role.equals("user")) {
            getActiveSessionsUser(hse).getAndDecrement();
        } else {
            getActiveSessionsAdmin(hse).getAndDecrement();
        }
    }

    private AtomicLong getActiveSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION);
    }

    private AtomicLong getActiveSessionsUser(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_USER);
    }

    private AtomicLong getActiveSessionsAdmin(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ADMIN);
    }
}
