package com.luxoft.courses.javaWeb;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {
    private static Map<String, List<String>> map = new HashMap<>();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_USER, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ADMIN, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_REQUEST, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_REQUEST_POST, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_REQUEST_GET, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_REQUEST_OTHER, new AtomicLong(0));
        parserXML(sce);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION);
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_USER);
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ADMIN);
        sce.getServletContext().removeAttribute(Constants.HTTP_REQUEST);
        sce.getServletContext().removeAttribute(Constants.HTTP_REQUEST_POST);
        sce.getServletContext().removeAttribute(Constants.HTTP_REQUEST_GET);
        sce.getServletContext().removeAttribute(Constants.HTTP_REQUEST_OTHER);

    }

    public void parserXML(ServletContextEvent servletContextEvent) {
        String dir = servletContextEvent.getServletContext().getRealPath("/");
        String xmlName = servletContextEvent.getServletContext().getInitParameter("users");
        File xmlFile = new File(dir + "META-INF\\" + xmlName);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFile);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getDocumentElement().getChildNodes();
            String name, password, role;
            for (int tmp = 0; tmp < nodeList.getLength(); tmp++) {
                Node node = nodeList.item(tmp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    NamedNodeMap attrs = node.getAttributes();
                    name = attrs.item(0).getNodeValue();
                    password = attrs.item(1).getNodeValue();
                    role = attrs.item(2).getNodeValue();
                    addToMap(name, password, role);
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void addToMap(String name, String password, String role) {
        List<String> list = new ArrayList<>();
        list.add(password);
        list.add(role);
        map.put(name, list);
    }

    public static Map<String, List<String>> getMap() {
        return Collections.unmodifiableMap(map);
    }
}
