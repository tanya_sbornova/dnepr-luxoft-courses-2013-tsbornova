package com.luxoft.courses.javaWeb;

public final class Constants {

    public final static String ACTIVE_SESSION = "ACTIVE_SESSION";
    public final static String ACTIVE_SESSION_USER = "ACTIVE_SESSION_USER";
    public final static String ACTIVE_SESSION_ADMIN = "ACTIVE_SESSION_ADMIN";
    public final static String HTTP_REQUEST = "HTTP_REQUEST";
    public final static String HTTP_REQUEST_POST = "HTTP_REQUEST_POST";
    public final static String HTTP_REQUEST_GET = "HTTP_REQUEST_GET";
    public final static String HTTP_REQUEST_OTHER = "HTTP_REQUEST_OTHER";
}
