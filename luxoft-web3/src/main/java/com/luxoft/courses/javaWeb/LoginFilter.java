package com.luxoft.courses.javaWeb;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class LoginFilter implements Filter {
    static Map<String, List<String>> map = ApplicationContextListener.getMap();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse, FilterChain filterChain)
            throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        String userName, password, role;
        if (session != null && session.getAttribute("name") != null) {
            userName = (String) session.getAttribute("name");
            role = (String) session.getAttribute("role");
        } else {
            userName = request.getParameter("name");
            password = request.getParameter("password");
            if (!isUserExist(userName, password)) {
                request.setAttribute("error", "Wrong login or password");
                request.setAttribute("name", userName);
                request.setAttribute("password", password);
                response.sendRedirect("index.html");
                return;
            }
            request.getSession().setAttribute("name", request.getParameter("name"));
            role = userRole(userName);
            if (role != null) {
                request.getSession().setAttribute("role", role);
            }
        }
        if (role != null) {
            if (role.equals("user")) {
                request.getRequestDispatcher("user").forward(request, response);
            } else if (role.equals("admin")) {
                request.getRequestDispatcher("/admin/sessionData").forward(request, response);
            }
        }
    }

    @Override
    public void destroy() {
    }

    public static boolean isUserExist(String userName, String password) {

        List<String> list = map.get(userName);
        if (list == null) {
            return false;
        }
        String passwordInMap = list.get(0);
        return (passwordInMap != null && passwordInMap.equals(password)) ? true : false;
    }

    public static String userRole(String userName) {

        List<String> list = map.get(userName);
        if (list == null) {
            return null;
        }
        return list.get(1);
    }
}
