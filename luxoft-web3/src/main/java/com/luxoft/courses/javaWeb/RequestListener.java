package com.luxoft.courses.javaWeb;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;

public class RequestListener implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        HttpServletRequest request = (HttpServletRequest) servletRequestEvent.getServletRequest();
        getRequests(servletRequestEvent).getAndIncrement();
        if (request.getMethod().equalsIgnoreCase("POST")) {
            getRequestsPost(servletRequestEvent).getAndIncrement();
        } else if (request.getMethod().equalsIgnoreCase("GET")) {
            getRequestsGet(servletRequestEvent).getAndIncrement();
        } else {
            getRequestsOther(servletRequestEvent).getAndIncrement();
        }
    }

    private AtomicLong getRequests(ServletRequestEvent servletRequestEvent) {
        return (AtomicLong) servletRequestEvent.getServletContext().getAttribute(Constants.HTTP_REQUEST);
    }

    private AtomicLong getRequestsPost(ServletRequestEvent servletRequestEvent) {
        return (AtomicLong) servletRequestEvent.getServletContext().getAttribute(Constants.HTTP_REQUEST_POST);
    }

    private AtomicLong getRequestsGet(ServletRequestEvent servletRequestEvent) {
        return (AtomicLong) servletRequestEvent.getServletContext().getAttribute(Constants.HTTP_REQUEST_GET);
    }

    private AtomicLong getRequestsOther(ServletRequestEvent servletRequestEvent) {
        return (AtomicLong) servletRequestEvent.getServletContext().getAttribute(Constants.HTTP_REQUEST_OTHER);
    }
}
