<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="com.luxoft.courses.javaWeb.*"%>
<html>
<head>
    <title>Session Data</title>
    <meta charset="UTF-8"/>
    <link href="${pageContext.request.contextPath}/styles/layout.css" rel="stylesheet">
</head>

<body>
<a href="logout" >logout</a>
<div id="table">
    <table>
    <tr>
    <th>Parameter</th>
    <th>Value</th>
    <tr>
    <td>Active Sessions</td>
    <td>${ACTIVE_SESSION}</td>
    </tr>
    <tr>
    <td>Active Sessions (ROLE user)</td>
    <td>${ACTIVE_SESSION_USER}</td>
    </tr>
    <tr>
    <td>Active Sessions (ROLE admin)</td>
    <td>${ACTIVE_SESSION_ADMIN}</td>
    </tr>
    <tr>
    <td>Total Count of HTTPRequests</td>
    <td>${HTTP_REQUEST}</td>
    </tr>
    <tr>
    <td>Total Count of POST HTTPRequests</td>
    <td>${HTTP_REQUEST_POST}</td>
    </tr>
    <tr>
    <td>Total Count of GET HTTPRequests</td>
    <td>${HTTP_REQUEST_GET}</td>
    </tr>
    <tr>
    <td>Total Count of Other HTTPRequests</td>
    <td>${HTTP_REQUEST_OTHER}</td>
    </tr>
</div>
</body>
</html>