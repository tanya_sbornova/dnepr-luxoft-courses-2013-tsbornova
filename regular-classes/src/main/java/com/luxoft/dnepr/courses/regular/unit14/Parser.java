package com.luxoft.dnepr.courses.regular.unit14;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    private static final String EXPRESSION_PATTERN = "[^\\s\\r\\n\\t.,:;?!-]*";
    private static final Pattern pattern = Pattern.compile(EXPRESSION_PATTERN);

    private Set<String> wordStorage;
    private Scanner in;

    public Parser(String fileName) {
        in = new Scanner(getClass().getResourceAsStream(fileName), "UTF-8");
        wordStorage = Storage.getWordStorage();
    }

    public void parse() {
        String str;
        while (in.hasNextLine()) {
            str = in.nextLine();
            if (isStringNotEmpty(str)) {
                wordStorage.addAll(tokenString(str));
            }
        }
    }

    private List<String> tokenString(String stringToken) {
        List<String> words = new ArrayList<>();
        String word;
        Matcher match = pattern.matcher(stringToken);
        while (match.find()) {
            word = match.group().toLowerCase();
            if (isWordNotShort(word)) {
                words.add(word);
            }
        }
        return words;
    }

    private boolean isStringNotEmpty(String str) {
        return (str.length() > 0);
    }

    private boolean isWordNotShort(String str) {
        return (str.length() > 3);
    }
}