package com.luxoft.dnepr.courses.regular.unit14;

import java.util.*;

public class Storage {
    private final static Set<String> wordStorage = new HashSet<>();

    public static Set<String> getWordStorage() {
        return wordStorage;
    }

    public static List<String> getWordList() {
        List<String> wordsList = new ArrayList<>();
        wordsList.addAll(wordStorage);
        Collections.shuffle(wordsList);
        return Collections.unmodifiableList(wordsList);
    }
}

