package com.luxoft.dnepr.courses.regular.unit14;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class WindowDialog extends JFrame {
    private final static int SIZE_X = 500;
    private final static int SIZE_Y = 250;
    private final static int SPACE_COMPONENTS = 5;
    private final static int SPACE_GROUP_COMPONENTS = 12;
    private final static int ROWS_GRID_LAYOUT = 1;
    private final static int COLUMNS_GRID_LAYOUT = 2;
    private final static int SPACE_X_GRID_LAYOUT = 5;
    private final static int SPACE_Y_GRID_LAYOUT = 0;
    private final static int FONT_SIZE_LABEL = 13;
    private final static int FONT_SIZE_WORD = 20;
    private final static String FONT_TYPE = "Arial";

    private List<String> wordStorage = Storage.getWordList();
    private int known = 0, unknown = 0, numberWord = 1;
    private JLabel wordField;

    public WindowDialog() {
        super("Linguistic analizator - Autor Tushar Brahmacobalol");
        createWindow();
    }

    private void createWindow() {
        setLocation(SIZE_X, SIZE_Y);
        add(createGUI());
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                new ButtonListener().result();
            }
        });
        pack();
        setVisible(true);
    }

    private JLabel createLabel(String text, JPanel panel, int fontSize) {
        JLabel label = new JLabel(text);
        label.setFont(new Font(FONT_TYPE, Font.PLAIN, fontSize));
        addToPanel(panel, label);
        return label;
    }

    private void createButton(String text, JPanel panel) {
        JButton button = new JButton(text);
        addToPanel(panel, button);
        button.addActionListener(new ButtonListener());
    }

    private void addToPanel(JPanel panel, Component component) {
        panel.add(component);
    }

    private JPanel createGUI() {
        JPanel mainPanel = BoxLayoutUtils.createVerticalPanel();
        mainPanel.setBorder(BorderFactory.createEmptyBorder(SPACE_GROUP_COMPONENTS, SPACE_GROUP_COMPONENTS, SPACE_GROUP_COMPONENTS, SPACE_GROUP_COMPONENTS));

        JPanel wordPanel = BoxLayoutUtils.createVerticalPanel();
        JLabel wordLabel = createLabel("Do you know translation of this word?", wordPanel, FONT_SIZE_LABEL);
        addToPanel(wordPanel, BoxLayoutUtils.createVerticalStrut(SPACE_COMPONENTS));
        wordField = createLabel(wordStorage.get(numberWord), wordPanel, FONT_SIZE_WORD);
        addToPanel(wordPanel, BoxLayoutUtils.createVerticalStrut(SPACE_GROUP_COMPONENTS));

        JPanel flowPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel gridPanel = new JPanel(new GridLayout(ROWS_GRID_LAYOUT, COLUMNS_GRID_LAYOUT, SPACE_X_GRID_LAYOUT, SPACE_Y_GRID_LAYOUT));

        createButton("Help", gridPanel);
        createButton("Yes", gridPanel);
        createButton("No", gridPanel);

        BoxLayoutUtils.setGroupAlignmentX(Component.LEFT_ALIGNMENT, wordPanel, mainPanel, flowPanel);
        BoxLayoutUtils.setGroupAlignmentY(Component.CENTER_ALIGNMENT, wordLabel, wordField);

        addToPanel(flowPanel, gridPanel);
        addToPanel(mainPanel, wordPanel);
        addToPanel(mainPanel, BoxLayoutUtils.createVerticalStrut(12));
        addToPanel(mainPanel, flowPanel);
        return mainPanel;
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (event.getActionCommand().equals("Help")) {
                showMessageWindow("Answer yes/no on questions, your English knowledge will be given to you");
                return;
            } else if (event.getActionCommand().equals("Yes")) {
                known++;
            } else {
                unknown++;
            }
            showNextWord();
        }

        private void showNextWord() {
            numberWord++;
            if (numberWord < wordStorage.size()) {
                wordField.setText(wordStorage.get(numberWord));
            } else {
                result();
            }
        }

        private void showMessageWindow(String message) {
            JOptionPane.showMessageDialog(null, message, null, JOptionPane.INFORMATION_MESSAGE);
        }

        private void result() {
            dispose();
            int words = wordStorage.size() * (known + 1) / (known + unknown + 1);
            if (known != 0 || unknown != 0) {
                showMessageWindow("Your estimated vocabulary is " + words + " words");
            }
            System.exit(0);
        }
    }

    public static void main(String[] args) {

        new Parser("sonnets.txt").parse();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new WindowDialog();
            }
        });
    }
}

