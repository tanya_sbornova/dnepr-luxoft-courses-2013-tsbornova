package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct {

    private boolean nonAlcoholic;

    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        this.nonAlcoholic = nonAlcoholic;
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    public boolean isNonAlcoholic() {
        return nonAlcoholic;
    }

    public void setNonAlcoholic(boolean nonAlcoholic) {
        this.nonAlcoholic = nonAlcoholic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        if (nonAlcoholic != beverage.nonAlcoholic) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nonAlcoholic ? 1 : 0);
        return result;
    }
}
