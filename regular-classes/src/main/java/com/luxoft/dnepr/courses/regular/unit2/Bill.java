package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    private List<Product> listCompositeProduct = new ArrayList<>();

    private CompositeProduct getCompositeProduct(Product product) {

        CompositeProduct tempCompositeProduct;

        for (Product nextProduct : listCompositeProduct) {
            tempCompositeProduct = (CompositeProduct) nextProduct;
            if (tempCompositeProduct.getProduct().equals(product)) {
                return tempCompositeProduct;
            }
        }
        return null;
    }

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        CompositeProduct tempCompositProduct;

        tempCompositProduct = getCompositeProduct(product);
        if (tempCompositProduct == null) {
            tempCompositProduct = new CompositeProduct();
            listCompositeProduct.add(tempCompositProduct);
        }
        tempCompositProduct.add(product);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     * <p/>
     * /@return
     */
    public double summarize() {

        double totalCost = 0;

        for (Product nextProduct : listCompositeProduct) {
            totalCost += nextProduct.getPrice();
        }

        return totalCost;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     * <p/>
     * /@return
     */
    public List<Product> getProducts() {

        Collections.sort(listCompositeProduct, new ListComparator());

        return listCompositeProduct;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

    static class ListComparator implements Comparator<Product> {

        public int compare(Product compositeProduct1, Product compositeProduct2) {
            double price1, price2;

            price1 = compositeProduct1.getPrice();
            price2 = compositeProduct2.getPrice();

            if (price1 < price2) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
