package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct {

    private Date publicationDate;

    public Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        this.publicationDate = publicationDate;
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (publicationDate != null ? !publicationDate.equals(book.publicationDate) : book.publicationDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (publicationDate != null ? publicationDate.hashCode() : 0);
        return result;
    }

    @Override
    public Product clone() throws CloneNotSupportedException {

        Book clone = (Book) super.clone();
        clone.publicationDate = (Date) publicationDate.clone();
        return clone;
    }
}
