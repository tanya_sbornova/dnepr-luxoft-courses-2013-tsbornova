package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {

    static final double DISCOUNT_2ITEMS = 0.05;
    static final double DISCOUNT_3ITEMS = 0.1;

    private List<Product> childProducts = new ArrayList<>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    public Product getProduct() {

        if (childProducts.isEmpty()) {
            return null;
        }
        return childProducts.get(0);
    }

    @Override
    public String getCode() {

        if (childProducts.isEmpty()) {
            return null;
        }
        return childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {

        if (childProducts.isEmpty()) {
            return null;
        }
        return childProducts.get(0).getName();
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        double discount = 0, totalPrice = 0;

        if (getAmount() == 2) {
            discount = DISCOUNT_2ITEMS;
        } else if (getAmount() >= 3) {
            discount = DISCOUNT_3ITEMS;
        }
        for (Product product : childProducts) {
            totalPrice += product.getPrice() - product.getPrice() * discount;
        }
        return totalPrice;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
