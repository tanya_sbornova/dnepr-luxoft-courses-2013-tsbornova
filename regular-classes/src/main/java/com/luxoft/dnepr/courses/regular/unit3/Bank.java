package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Bank implements BankInterface {

    private final static int DIGITS_AFTER_POINT = 2;
    private Map<Long, UserInterface> users = new HashMap<>();

    public Bank(String expectedJavaVersion) {

        String actualJavaVersion = System.getProperty("java.version");

        if (!actualJavaVersion.equals(expectedJavaVersion)) {

            String errorMessage = "Illegal Java version. Expected: " + expectedJavaVersion + ", actual: " + actualJavaVersion;
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, errorMessage);
        }
    }

    public Map<Long, UserInterface> getUsers() {

        return users;
    }

    public void setUsers(Map<Long, UserInterface> users) {

        this.users = users;
    }

    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {

        BigDecimal amountToWithdraw;
        BigDecimal amountInWallet;
        BigDecimal amountToTransfer;
        BigDecimal maxAmount;
        String errorMessage;
        UserInterface fromUser, toUser;
        WalletInterface walletFromUser, walletToUser;

        if (amount == null) {
            throw new NullValueException("Illegal argument amount in Bank.makeMoneyTransaction()");
        }

        checkUser(fromUserId);
        checkUser(toUserId);

        fromUser = users.get(fromUserId);
        toUser = users.get(toUserId);

        for (UserInterface user : Arrays.asList(fromUser, toUser)) {
            if (!isStatusActive(user.getWallet())) {
                errorMessage = "User '" + user.getName() + "' wallet is blocked";
                throw new TransactionException(errorMessage);
            }
        }

        walletFromUser = fromUser.getWallet();
        try {
            walletFromUser.checkWithdrawal(amount);

        } catch (WalletIsBlockedException exception) {
            errorMessage = "User '" + fromUser.getName() + "' wallet is blocked";
            throw new TransactionException(errorMessage);

        } catch (InsufficientWalletAmountException exception) {
            amountToWithdraw = exception.getAmountToWithdraw();
            amountToWithdraw = roundBigDecimal(amountToWithdraw);
            amountInWallet = exception.getAmountInWallet();
            amountInWallet = roundBigDecimal(amountInWallet);
            errorMessage = "User '" + fromUser.getName() + "' has insufficient funds (" + amountInWallet + " < " + amountToWithdraw + ")";
            throw new TransactionException(errorMessage);
        }

        walletToUser = toUser.getWallet();

        try {
            walletToUser.checkTransfer(amount);

        } catch (WalletIsBlockedException exception) {
            errorMessage = "User '" + toUser.getName() + "' wallet is blocked";
            throw new TransactionException(errorMessage);

        } catch (LimitExceededException exception) {
            amountToTransfer = exception.getAmountToTransfer();
            amountToTransfer = roundBigDecimal(amountToTransfer);
            amountInWallet = exception.getAmountInWallet();
            amountInWallet = roundBigDecimal(amountInWallet);
            maxAmount = walletToUser.getMaxAmount();
            maxAmount = roundBigDecimal(maxAmount);
            errorMessage = "User '" + toUser.getName();
            errorMessage += "' wallet limit exceeded (" + amountInWallet + " + " + amountToTransfer + " > " + maxAmount + ")";
            throw new TransactionException(errorMessage);
        }
        walletFromUser.withdraw(amount);
        walletToUser.transfer(amount);
    }

    private BigDecimal roundBigDecimal(BigDecimal value) {

        return value.setScale(DIGITS_AFTER_POINT, BigDecimal.ROUND_HALF_UP);
    }

    private void checkUser(Long userId) throws NoUserFoundException {

        if (!users.containsKey(userId)) {

            String errorMessage = "User '" + userId + "' not found";
            throw new NoUserFoundException(userId, errorMessage);
        }
    }

    private boolean isStatusActive(WalletInterface wallet) {

        if (wallet.getStatus() == WalletStatus.BLOCKED) {
            return false;
        } else {
            return true;
        }
    }

}

