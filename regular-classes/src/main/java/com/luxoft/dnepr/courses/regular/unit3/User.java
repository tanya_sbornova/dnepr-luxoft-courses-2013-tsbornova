package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.NullValueException;

public class User implements UserInterface {

    private Long id;
    private String name;
    private WalletInterface wallet;

    public User(Long id, String name, WalletInterface wallet) {

        if (wallet == null) {
            throw new NullValueException("Illegal argument wallet in User constructor");
        }
        this.id = id;
        this.name = name;
        this.wallet = wallet;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public WalletInterface getWallet() {
        return wallet;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWallet(WalletInterface wallet) {
        if (wallet == null) {
            throw new NullValueException("Illegal argument wallet in User.setWallet()");
        }
        this.wallet = wallet;
    }
}
