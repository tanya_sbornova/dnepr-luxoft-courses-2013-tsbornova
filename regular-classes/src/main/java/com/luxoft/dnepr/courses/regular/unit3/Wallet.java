package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NullValueException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {

    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {

        if (amount == null) {
            throw new NullValueException("Illegal argument amount in Wallet constructor");
        }
        if (maxAmount == null) {
            throw new NullValueException("Illegal argument maxAmount in Wallet constructor");
        }
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public WalletStatus getStatus() {
        return status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAmount(BigDecimal amount) {
        if (amount == null) {
            throw new NullValueException("Illegal argument amount in Wallet.setAmount()");
        }
        this.amount = amount;
    }

    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        if (maxAmount == null) {
            throw new NullValueException("Illegal argument maxAmount in Wallet.setMaxAmount()");
        }
        this.maxAmount = maxAmount;
    }

    private void checkStatus() throws WalletIsBlockedException {

        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id, " wallet is blocked");
        }
    }


    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {

        checkStatus();

        if (amountToWithdraw.compareTo(amount) > 0) {
            String errorMessage = "Insufficient funds";
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, errorMessage);
        }

    }

    public void withdraw(BigDecimal amountToWithdraw) {

        amount = amount.subtract(amountToWithdraw);

    }

    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {

        BigDecimal temp;

        checkStatus();

        temp = amountToTransfer.add(amount);

        if (temp.compareTo(maxAmount) > 0) {

            String errorMessage = "Wallet limit exceeded";
            throw new LimitExceededException(id, amountToTransfer, amount, errorMessage);
        }
    }

    public void transfer(BigDecimal amountToTransfer) {

        amount = amount.add(amountToTransfer);

    }
}
