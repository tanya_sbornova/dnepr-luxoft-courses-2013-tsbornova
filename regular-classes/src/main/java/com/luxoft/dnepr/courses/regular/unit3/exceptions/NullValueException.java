package com.luxoft.dnepr.courses.regular.unit3.exceptions;

public class NullValueException extends RuntimeException {

    public NullValueException(String message) {

        super(message);
    }

}
