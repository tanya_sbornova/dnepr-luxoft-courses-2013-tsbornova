package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

public class EqualSet<E> implements Set<E> {

    private ArrayList<E> list;

    public EqualSet() {

        list = new ArrayList<>();
    }

    public EqualSet(Collection<? extends E> collection) {
        list = new ArrayList<>(Math.max(collection.size(), 10));
        addAll(collection);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object obj) {
        return list.contains(obj);
    }

    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] array) {
        return list.toArray(array);
    }

    @Override
    public boolean add(E elem) {

        if (list.contains(elem)) {
            return false;
        }
        list.add(elem);
        return true;
    }

    @Override
    public boolean remove(Object obj) {

        if (!list.contains(obj)) {
            return false;
        }
        int index = list.indexOf(obj);
        list.remove(index);
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {

        return list.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {

       boolean isAdd = false;

        for (E elem : collection) {
            if (add(elem)) {
                isAdd = true;
            }
        }
        return isAdd;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {

        return list.retainAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {

        return list.removeAll(collection);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof EqualSet))
            return false;
        Collection collection = (Collection) obj;
        if (collection.size() != size())
            return false;
        try {
            return containsAll(collection);
        } catch (ClassCastException unused) {
            return false;
        } catch (NullPointerException unused) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hashCode = 0;

        Iterator<E> iterator = iterator();
        while (iterator.hasNext()) {
            E obj = iterator.next();
            if (obj != null)
                hashCode += obj.hashCode();
        }
        return hashCode;
    }
}
