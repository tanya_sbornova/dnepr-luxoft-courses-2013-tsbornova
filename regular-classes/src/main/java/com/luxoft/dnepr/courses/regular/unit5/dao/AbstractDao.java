package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

public abstract class AbstractDao<E extends Entity> implements IDao<E> {

    private Map<Long, Entity> entities;
//    private Map<Long, E> entities;
    private EntityStorage<E> generic;

    private final static long STARTING_ID = 0;

    public AbstractDao() {

//        generic = new EntityStorage<>();
//        entities = generic.getEntities();
        entities = EntityStorage.getEntities();

    }

    public EntityStorage<E> getEntityStorage() {

        return generic;
    }

    private Long nextId() {

        HashSet<Long> keySet = new HashSet<>(entities.keySet());

        return keySet.isEmpty() ? STARTING_ID : Collections.max(keySet);
    }

    private boolean isIdExist(Long id) {

        return entities.containsKey(id);

    }

    public E save(E entity) {
        Long id;

        if (entity == null) {
            return null;
        }
        id = entity.getId();
        if (isIdExist(id)) {
            throw new UserAlreadyExist("User " + id + " already exist");
        }
        if (id == null) {
            id = nextId()+1;
            entity.setId(id);
        }
        entities.put(id, entity);

        return entity;
    }

    public E update(E entity) {
        Long id;

        if (entity == null) {
            return null;
        }
        id = entity.getId();
        if (id == null || !isIdExist(id)) {
            throw new UserNotFound("User " + id + " not found");
        }
        entities.put(id, entity);
        return entity;
    }

//    public E get(long id) {
    public E get(long id) {

        return (E)entities.get(id);
    }

    public boolean delete(long id) {

        return entities.remove(id) != null;
    }

}
