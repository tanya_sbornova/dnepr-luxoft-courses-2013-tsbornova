package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

public class EntityStorage2<E extends Entity> {

    final Map<Long, E> entities = new HashMap();

    public EntityStorage2() {
    }

    public Map<Long, E> getEntities() {

        return entities;
    }
}
