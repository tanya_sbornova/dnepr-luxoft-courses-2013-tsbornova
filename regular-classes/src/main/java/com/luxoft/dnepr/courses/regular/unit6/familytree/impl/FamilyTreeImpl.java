package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.*;

public class FamilyTreeImpl implements FamilyTree {
	
	private static final long serialVersionUID = 3057396458981676327L;
	private Person root;
	private transient long creationTime;

    private FamilyTreeImpl(Person root, long creationTime) {
		this.root = root;
		this.creationTime = creationTime;
	}
	
	public static FamilyTree create(Person root) {
		return new FamilyTreeImpl(root, System.currentTimeMillis());
	}
	
	@Override
	public Person getRoot() {
		return root;
	}
	
	@Override
	public long getCreationTime() {
		return creationTime;
	}

    private void writeObject(ObjectOutputStream ois) throws IOException {
        Writer out = new BufferedWriter(new OutputStreamWriter(ois));

        out.write("{");
        if (root != null) {
            out.write("\"root\": \n");
            savePerson(out, root);
        }
        out.write("}\n");
        out.flush();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {

        Parser parser = new Parser(new Lexer(in));
        root = parser.parseJSON();
//        this.root =  (Person) in.readObject();
    }

    private static void savePerson(Writer out, Person person) throws IOException {

        out.write("{");
        if (person.getName() != null) {
            out.write("\"name\":\"");
            out.write(person.getName());
            out.write("\",");
        }
        if (person.getEthnicity() != null) {
            out.write("\"ethnicity\":\"");
            out.write(person.getEthnicity());
            out.write("\",");
        }
        if (person.getFather() != null) {
            out.write("\n\"father\":");
            savePerson(out, person.getFather());
            out.write(",\n");
        }
        if (person.getMother() != null) {
            out.write("\n\"mother\":");
            savePerson(out, person.getMother());
            out.write(",\n");
        }
        if (person.getGender() != null) {
            out.write("\"gender\":\"");
            out.write(person.getGender().toString());
            out.write("\",");
        }
        out.write("\"age\":\"");
        out.write(String.valueOf(person.getAge()));
        out.write("\"");
        out.write("}");
    }
}
