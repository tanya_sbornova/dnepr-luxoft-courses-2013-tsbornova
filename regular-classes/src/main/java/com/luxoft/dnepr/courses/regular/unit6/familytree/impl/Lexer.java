package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import java.io.IOException;
import java.io.ObjectInput;

class Lexer {

    static final int VALUE = 0x01;
    static final int BRACKET = 0x02;
    static final int COLON = 0x04;
    static final int COMMA = 0x08;

    private ObjectInput reader;
    private int nextChar;

    Lexer(ObjectInput input) throws IOException {

        reader = input;
        nextChar = input.read();
    }

    Token next() throws IOException {

        int c = skipWhiteSpaces();
        if (c < 0) {
            return null;
        }
        if (isColon(c)) {
            nextChar = reader.read();
            return new Token(COLON, String.valueOf((char) c));
        }
        if (isComma(c)) {
            nextChar = reader.read();
            return new Token(COMMA, String.valueOf((char) c));
        }

        if (isBracket(c)) {
            nextChar = reader.read();
            return new Token(BRACKET, String.valueOf((char) c));
        }
        return readValue(c);
    }

    private int skipWhiteSpaces() throws IOException {
        int c = nextChar;
        while (c >= 0 && Character.isWhitespace(c)) {
            c = reader.read();
        }
        return c;
    }

    private Token readValue(int c) throws IOException {

        StringBuilder result = new StringBuilder();
        if (!isQuota(c)) {
            throw new IOException("JSON format error. Require quote \"");
        }
        c = reader.read();
        while (c >= 0 && c != '\n' && !isQuota(c)) {
            result.appendCodePoint(c);
            c = reader.read();
        }
        if (isQuota(c)) {
            c = reader.read();
        }
        nextChar = c;
        return new Token(VALUE, result.toString());
    }

    private boolean isColon(int c) {
        return c == ':';
    }

    private boolean isComma(int c) {
        return c == ',';
    }

    private boolean isBracket(int c) {
        return c == '{' || c == '}';
    }

    private boolean isQuota(int c) {
        return c == '"';
    }
}

class Token {
    private int type;
    private String value;

    public Token(int type, String value) {
        super();
        this.type = type;
        this.value = value;
    }

    public boolean isColon() {
        return (type & Lexer.COLON) > 0;
    }

    public boolean isComma() {
        return (type & Lexer.COMMA) > 0;
    }

    public boolean isValue() {
        return (type & Lexer.VALUE) > 0;
    }

    public boolean isBracket() {
        return (type & Lexer.BRACKET) > 0;
    }

    public String getValue() {
        return value;
    }

    public boolean isEqual(String value) {
        return value != null && value.equals(this.value);
    }

    @Override
    public String toString() {
        return value;
    }
}

