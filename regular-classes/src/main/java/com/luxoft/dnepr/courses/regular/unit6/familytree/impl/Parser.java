package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Parser {

    private static final String FIELD_FAMILY_TREE = "root";
    private static final List<String> FIELDS_PERSON = Arrays.asList(new String[]{"name", "ethnicity", "father", "mother", "gender", "age"});
    private static final List<String> PARENTS = Arrays.asList(new String[]{"father", "mother"});

    private List<Token> tokens;
    private int index, brackets = 0;

    Parser(Lexer lexer) throws IOException {
        tokens = new ArrayList<>();
        Token token;
        while ((token = lexer.next()) != null) {
            tokens.add(token);
        }
        index = 0;
    }

    public Person parseJSON() throws IOException {
        Token tok = nextToken();
        Person person = null;
//        FamilyTree familyTree = FamilyTreeImpl.create(person);

        if (tok != null && tok.isBracket() && tok.isEqual("{")) {
            brackets++;
            tok = nextToken();
            if (tok != null && tok.isValue() && tok.isEqual(FIELD_FAMILY_TREE)) {
                person = parseFamilyTree();
//                familyTree = FamilyTreeImpl.create(person);
                tok = nextToken();
            }
        }
        if (tok != null && tok.isBracket() && tok.isEqual("}")) {
            brackets--;
        }
        checkBrackets();
        return person;
    }

    private Person parseFamilyTree() throws IOException {

        Token tok = nextToken();
        Person person = null;

        if (tok.isColon()) {
            tok = nextToken();
            if (tok != null && tok.isBracket() && tok.isEqual("{")) {
                brackets++;
                person = parsePerson();
            }
        } else {
            throw new IOException("JSON format error ");
        }
        return  person;
    }

    private Person parsePerson() throws IOException {
        Token tok = nextToken();
        String field, value;
        Person person = new PersonImpl();

        while (tok != null) {
            field = acceptField(tok);
            acceptColon();
            value = acceptValue(person, field);
            setPerson(person, field, value);
            if (!acceptComma()) {
                brackets--;
                break;
            }
            tok = nextToken();
        }
        return person;
    }

    private String acceptField(Token tok) throws IOException {

        if (!tok.isValue() || !FIELDS_PERSON.contains(tok.getValue())) {
            throw new IOException(errorMessage(tok, " is not correct field of the Person object"));
        }
        return tok.getValue();
    }

    private void acceptColon() throws IOException {
        Token tok;

        tok = nextToken();
        if (tok == null || !tok.isColon()) {
            throw new IOException(errorMessage(tok, " is not colon(:)"));
        }
    }

    private String acceptValue(Person person, String field) throws IOException {
        Token tok;
        Person newPerson;

        tok = nextToken();
        if (tok != null && tok.isBracket() && tok.isEqual("{") && (PARENTS.contains(field))) {
            brackets++;
            newPerson = parsePerson();
            setPerson(person, field, newPerson);
//            setPerson(person, field, person.);
        } else if (tok == null || !tok.isValue()) {
            if (tok != null) {
                throw new IOException(errorMessage(tok, " is not correct value of the Person object"));
            }
        }
        return tok.getValue();
    }

    private boolean acceptComma() throws IOException {
        Token tok;

        tok = nextToken();
        if (tok == null || !(tok.isComma() || tok.isBracket() && tok.isEqual("}"))) {
            throw new IOException(errorMessage(tok, " is not comma(,) or bracket(})"));
        }
        return tok.isComma();
    }

    private String errorMessage(Token tok, String message) {
        StringBuilder errorMessage = new StringBuilder("JSON format error. ");
        if (tok != null) {
            errorMessage.append("Element ");
            errorMessage.append(String.valueOf(index));
            errorMessage.append("  ");
            errorMessage.append(tok.getValue());
            errorMessage.append(message);
        }
        return errorMessage.toString();
    }

    private Token nextToken() {
        if (index >= tokens.size()) {
            return null;
        }
        return tokens.get(index++);
    }

    private void setPerson(Person person, String field, Person value) {

        switch (field) {

            case "father":
                ((PersonImpl) person).setFather(value);
                break;
            case "mother":
                ((PersonImpl) person).setMother(value);
        }
    }

    private void setPerson(Person person, String field, String value) {

        switch (field) {

            case "name":
                ((PersonImpl) person).setName(value);
                break;
            case "ethnicity":
                ((PersonImpl) person).setEthnicity(value);
                break;
            case "gender":
                ((PersonImpl) person).setGender(Gender.valueOf(value));
                break;
            case "age":
                ((PersonImpl) person).setAge(Integer.parseInt(value));
        }
    }

    private void checkBrackets() throws IOException {

        if (brackets > 0) {
            throw new IOException("JSON format error. Require } ");
        } else if (brackets < 0) {
            throw new IOException("JSON format error. Require { ");
        }
    }
}


