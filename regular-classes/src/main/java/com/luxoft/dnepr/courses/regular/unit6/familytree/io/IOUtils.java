package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;

import java.io.*;

public class IOUtils {

    private IOUtils() {
    }

    public static FamilyTree load(String filename) throws IOException, ClassNotFoundException {

        FamilyTree familyTree;
        try (InputStream is = new FileInputStream(filename)) {
            familyTree = load(is);
        }
        return familyTree;
    }

    public static FamilyTree load(InputStream is) throws IOException, ClassNotFoundException {

        FamilyTree familyTree;
        try (ObjectInputStream ois = new ObjectInputStream(is)) {
            familyTree = (FamilyTree) ois.readObject();
        }
        return familyTree;
    }

    public static void save(String filename, FamilyTree familyTree) throws IOException {

        try (OutputStream os = new FileOutputStream(filename)) {
            save(os, familyTree);
        }
    }

    public static void save(OutputStream os, FamilyTree familyTree) throws IOException {

        try (ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeObject(familyTree);
            oos.flush();
        }
    }
}
