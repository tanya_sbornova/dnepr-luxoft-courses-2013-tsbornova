package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {

    private static final int QUEUE_SIZE = 50;
    public static final File DUMMY = new File("");

    private int maxNumberOfThreads;
    private final CountDownLatch latch;
    private BlockingQueue<File> queue = new LinkedBlockingQueue<>(QUEUE_SIZE);
    private WordStorage wordStorage = new WordStorage();
    List<File> processedFiles = new ArrayList<>();
    private File rootFolder;


    public FileCrawler(String rootFolder, int maxNumberOfThreads) {

        this.rootFolder = rootFolder == null ? new File(System.getProperty("user.home")) : new File(rootFolder);
        this.maxNumberOfThreads = maxNumberOfThreads < 2 ? 2 : maxNumberOfThreads;
        latch = new CountDownLatch(this.maxNumberOfThreads - 1);
    }

    private boolean isFileTxt(File file) {

        String fileName = file.getName();
        return fileName.endsWith("txt");
    }

    private void crawl(File root) throws InterruptedException {

        File[] entries = root.listFiles();
        if (entries != null) {
            for (File entry : entries) {
                if (entry.isDirectory()) {
                    crawl(entry);
                } else if (isFileTxt(entry)) {
                    processedFiles.add(entry);
                    queue.put(entry);
                }
            }
        }
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {

        for (int i = 1; i < maxNumberOfThreads; i++) {
            new Thread(new FileParser(queue, wordStorage, latch)).start();
        }
        try {
            crawl(rootFolder);
            queue.put(DUMMY);
            latch.await();
        } catch (InterruptedException exception) {
            throw new RuntimeException(exception.getMessage());
        }
        return new FileCrawlerResults(processedFiles, wordStorage.getWordStatistics());
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }
}
