package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileParser implements Runnable {

    private static final String EXPRESSION_PATTERN = "([a-zA-Z0-9а-яА-яёЁ])+";
    private static final Pattern pattern = Pattern.compile(EXPRESSION_PATTERN);

    private final CountDownLatch latch;
    private WordStorage wordStorage;
    private BlockingQueue<File> queue;


    public FileParser(BlockingQueue<File> queue, WordStorage wordStorage, CountDownLatch latch) {

        this.queue = queue;
        this.wordStorage = wordStorage;
        this.latch = latch;
    }

    @Override
    public void run() {
        File file;

        boolean done = false;
        while (!done) {
            try {
                file = queue.take();
            } catch (InterruptedException exception) {
                break;
            }
            if (file == FileCrawler.DUMMY) {
                try {
                    queue.put(FileCrawler.DUMMY);
                } catch (InterruptedException exception) {
                    break;
                }
                done = true;
            } else {
                parse(file);
            }
        }
        latch.countDown();
    }

    private void parse(File file) {

        List<String> listWords;
        String str;

        try (Scanner in = new Scanner(file, "UTF-8")) {
            while (in.hasNextLine()) {
                str = in.nextLine();
                listWords = tokenString(str);
                for (String strNext : listWords) {
                    wordStorage.save(strNext);
                }
            }
        } catch (FileNotFoundException exception) {
            throw new RuntimeException(exception.getMessage());
        }
    }

    private List<String> tokenString(String stringToken) {
        List<String> words = new ArrayList<>();

        Matcher match;
        match = pattern.matcher(stringToken);
        while (match.find()) {
            words.add(match.group());
        }
        return words;
    }
}
