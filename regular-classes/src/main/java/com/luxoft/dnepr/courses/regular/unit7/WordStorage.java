package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Represents word statistics storage.
 */
public class WordStorage {

    Map<String, AtomicInteger> wordStatistics;

    public WordStorage() {

        wordStatistics = new HashMap<>();
    }

    /**
     * Saves given word and increments count of occurrences.
     *
     * @param word
     */

    public synchronized void save(String word) {
        AtomicInteger count;

        count = wordStatistics.get(word);
        if (count == null) {
            wordStatistics.put(word, new AtomicInteger(1));
        } else {
            count.incrementAndGet();
        }
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {

        return Collections.unmodifiableMap(wordStatistics);
    }
}
