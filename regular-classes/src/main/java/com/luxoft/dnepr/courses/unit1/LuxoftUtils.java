package com.luxoft.dnepr.courses.unit1;

import java.math.BigInteger;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 05.10.13
 * Time: 22:13
 * To change this template use File | Settings | File Templates.
 */

public final class LuxoftUtils {

    private LuxoftUtils() {
    }

    public static String getMonthName(int monthOrder, String language) {

        if (language == null) {
            return "Unknown Language";
        }
        if (language.equals("en")) {
            switch (monthOrder) {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return "Unknown Month";
            }
        } else if (language.equals("ru")) {
            switch (monthOrder) {
                case 1:
                    return "Январь";
                case 2:
                    return "Февраль";
                case 3:
                    return "Март";
                case 4:
                    return "Апрель";
                case 5:
                    return "Май";
                case 6:
                    return "Июнь";
                case 7:
                    return "Июль";
                case 8:
                    return "Август";
                case 9:
                    return "Сентябрь";
                case 10:
                    return "Октябрь";
                case 11:
                    return "Ноябрь";
                case 12:
                    return "Декабрь";
                default:
                    return "Неизвестный месяц";
            }
        } else {
            return "Unknown Language";
        }
    }

    public static String binaryToDecimal(String binaryNumber) {

        char currentChar;
        BigInteger resultDecimal = BigInteger.valueOf(0);

        if ((binaryNumber == null) || binaryNumber.equals("")) {
            return "Not binary";
        }
        int lengthBinaryNumber = binaryNumber.length();
        for (int i = lengthBinaryNumber - 1, power = 0; i >= 0; i--, power++) {
            currentChar = binaryNumber.charAt(i);
            if ((currentChar != '0') && (currentChar != '1')) {
                return "Not binary";
            } else {
                resultDecimal = resultDecimal.add(BigInteger.valueOf(
                        (currentChar - '0')).multiply(
                        BigInteger.valueOf(2).pow(power)));
            }
        }
        return resultDecimal.toString();
    }

    public static String decimalToBinary(String decimalNumber) {

        final int maxLengthInt = 32;
        final int maxNegativeInt = -2147483648;

        char currentChar;
        boolean isNegative = false;
        boolean isNumberLessMinInt = false;

        BigInteger[] divideAndRemainder;
        StringBuilder result = new StringBuilder();

        if ((decimalNumber == null) || (decimalNumber.equals(""))) {
            return "Not decimal";
        }
        if (decimalNumber.charAt(0) == '-') {
            isNegative = true;
            decimalNumber = decimalNumber.substring(1);
        }
        if (decimalNumber.charAt(0) == '+') {
            decimalNumber = decimalNumber.substring(1);
        }
        int lengthDecimalNumber = decimalNumber.length();
        for (int i = 0; i < lengthDecimalNumber; i++) {
            currentChar = decimalNumber.charAt(i);
            if ((currentChar < '0') || (currentChar > '9')) {
                return "Not decimal";
            }
        }
        BigInteger bigDecimalNumber = new BigInteger(decimalNumber);
        if (isNegative) {
            if (bigDecimalNumber.equals(BigInteger.valueOf(0))) {
                isNegative = false;
            } else {
                if (bigDecimalNumber.negate().equals(
                        bigDecimalNumber.negate().min(
                                BigInteger.valueOf(maxNegativeInt)))) {
                    isNumberLessMinInt = true;
                }
                bigDecimalNumber = bigDecimalNumber.subtract(BigInteger
                        .valueOf(1));
            }
        }
        while (bigDecimalNumber.compareTo(BigInteger.valueOf(2)) >= 0) {
            divideAndRemainder = bigDecimalNumber.divideAndRemainder(BigInteger
                    .valueOf(2));
            result = result.append(divideAndRemainder[1].toString());
            bigDecimalNumber = divideAndRemainder[0];

        }
        result = result.append(bigDecimalNumber);
        result = result.reverse();
        if (isNegative) {
            for (int i = 0; i <= result.length() - 1; i++) {
                result.setCharAt(i, result.charAt(i) == '0' ? '1' : '0');
            }
            int addNegativeLenght = maxLengthInt - result.length() - 1;
            if (isNumberLessMinInt) {
                addNegativeLenght = 0;
            }
            for (int i = 0; i <= addNegativeLenght; i++) {
                result.insert(i, '1');
            }
        }
        return result.toString();
    }


    private static void swap(int[] array, int i, int j) {

        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static int[] sortArray(int[] array, boolean asc) {

        if (array == null) {
            return null;
        }

        int[] arraySort = new int[array.length];

        System.arraycopy(array, 0, arraySort, 0, array.length);

        for (int i = arraySort.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (asc && arraySort[j] > arraySort[j + 1]) {
                    swap(arraySort, j, j + 1);
                }
                if (!asc && arraySort[j] < arraySort[j + 1]) {
                    swap(arraySort, j, j + 1);
                }
            }
        }
        return arraySort;
    }
}