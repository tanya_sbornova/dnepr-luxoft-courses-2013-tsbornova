package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 12.10.13
 * Time: 10:50
 * To change this template use File | Settings | File Templates.
 */
public final class LuxoftUtils {

    private LuxoftUtils() {
    }

    private static boolean isNeedTransp(String str1, String str2, boolean asc) {
        if (asc && str1.compareTo(str2) < 0) {
            return true;
        }
        if (!asc && str1.compareTo(str2) >= 0) {
            return true;
        }
        return false;
    }

    private static void mergeSort(String[] array, int low, int high, boolean asc) {
        if (low + 1 < high) {
            int mid = (low + high) / 2;
            mergeSort(array, low, mid, asc);
            mergeSort(array, mid, high, asc);

            int size = high - low;
            String[] tempArray = new String[size];
            int i = low;
            int j = mid;
            for (int k = 0; k < size; k++) {
                if (j >= high || i < mid && isNeedTransp(array[i], array[j], asc)) {
                    tempArray[k] = array[i++];
                } else {
                    tempArray[k] = array[j++];
                }
            }
            System.arraycopy(tempArray, 0, array, low, size);
        }
    }

    public static String[] sortArray(String[] array, boolean asc) {

        String[] copyArray = array.clone();
        mergeSort(copyArray, 0, copyArray.length, asc);

        return copyArray;
    }

    public static double wordAverageLength(String str) {
        int sum = 0;
        int count = 0;

        String[] strArray = str.split("\\s+");
        for (String tempStr : strArray) {
            sum += tempStr.length();
            if (tempStr.length() > 0) {
                count++;
            }
        }
        if (count == 0) {
            return 0;
        }
        return (double) (sum * 1000 / count) / 1000;
    }

    public static String reverseWords(String str) {

        String tempMatch;
        int tempInd;

        StringBuilder strBuild = new StringBuilder(str);
        Pattern pattern = Pattern.compile("\\S+");
        Matcher match = pattern.matcher(str);
        while (match.find()) {
            tempMatch = match.group();
            tempInd = match.start();
            for (int i = tempMatch.length() - 1, j = 0; i >= 0; i--, j++) {
                strBuild.replace(tempInd + j, tempInd + j + 1, "" + tempMatch.charAt(i));
            }
        }
        return strBuild.toString();
    }

    public static char[] getCharEntries(String str) {

        final HashMap<Character, Integer> mapChar = new HashMap<Character, Integer>();
        TreeMap<Character, Integer> mapSortChar = new TreeMap<Character, Integer>(
                new Comparator() {

                    @Override
                    public int compare(Object obj1, Object obj2) {
                        int value1 = mapChar.get(obj1);
                        int value2 = mapChar.get(obj2);
                        if (value1 == value2) {
                            return ((Character) obj1).compareTo((Character) obj2);
                        } else {
                            return (value1 < value2) ? 1 : -1;
                        }
                    }
                });

        char temp;
        int tempValue;

        for (int i = 0; i < str.length(); i++) {
            temp = str.charAt(i);
            if (temp != ' ') {
                if (mapChar.containsKey(temp)) {
                    tempValue = mapChar.get(temp);
                    mapChar.put(temp, ++tempValue);
                } else {
                    mapChar.put(temp, 1);
                }
            }
        }

        int mapSize = mapChar.size();

        char[] rezultArray = new char[mapSize];

        mapSortChar.putAll(mapChar);
        Object[] tempArray = mapSortChar.keySet().toArray();
        int i = 0;
        for (Object c : tempArray) {
            rezultArray[i] = (Character) c;
            i++;
        }
        return rezultArray;
    }

    public static double calculateOverallArea(List<Figure> figures) {

        double sum = 0;

        for (Figure figure : figures) {
            sum += figure.calculateArea();
        }
        return sum;
    }
}
