package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 12.10.13
 * Time: 17:18
 * To change this template use File | Settings | File Templates.
 */
public abstract class Figure {
    public abstract double calculateArea();
}
