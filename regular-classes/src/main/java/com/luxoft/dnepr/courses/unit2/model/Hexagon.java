package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 12.10.13
 * Time: 17:51
 * To change this template use File | Settings | File Templates.
 */
public class Hexagon extends Figure {
    private double side;

    public Hexagon(double side) {
        this.side = side;
    }

    public double calculateArea() {
        return (3 * Math.sqrt(3) * side * side) / 2;
    }
}
