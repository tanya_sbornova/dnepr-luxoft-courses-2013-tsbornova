package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 12.10.13
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
public class Square extends Figure {
    private double side;

    public Square(double side) {
        this.side = side;
    }

    public double calculateArea() {
        return side * side;
    }
}
