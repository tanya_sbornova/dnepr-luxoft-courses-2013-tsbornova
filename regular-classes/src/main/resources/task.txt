1. Добавьте в имеющиеся исключения и ошибки следующие поля и конструкторы
(все исключения вызывают конструктор предка, который принимает String message):
1.1
IllegalJavaVersionError имеет приватные поля
String actualJavaVersion;
String expectedJavaVersion;
и конструктор
IllegalJavaVersionError(String actualJavaVersion, String expectedJavaVersion, String message)
вышеуказанные поля имеют get-еры
1.2 InsufficientWalletAmountException имеет приватные поля
Long walletId;
BigDecimal amountToWithdraw;
BigDecimal amountInWallet;
и конструктор
InsufficientWalletAmountException(Long walletId
            , BigDecimal amountToWithdraw
            , BigDecimal amountInWallet
            , String message)
вышеуказанные поля имеют get-еры
1.3 LimitExceededException имеет приватные поля
Long walletId;
BigDecimal amountToTransfer;
BigDecimal amountInWallet;
и конструктор
LimitExceededException(Long walletId
            , BigDecimal amountToTransfer
            , BigDecimal amountInWallet
            , String message)
вышеуказанные поля имеют get-еры
1.4 NoUserFoundException имеет приватные поля
Long userId
и конструктор
NoUserFoundException(Long userId, String message)
вышеуказанные поля имеют get-еры
1.5 TransactionException не имеет полей,
но имеет конструктор
TransactionException(String message)
1.6 WalletIsBlockedException имеет приватные поля
Long walletId
и конструктор
WalletIsBlockedException(Long walletId, String message)

2. Реализуйте класс com.luxoft.dnepr.courses.regular.Wallet, который наследует интерфейс WalletInterface.
- добавьте необходимые поля (руководствуясь геттерами из интерфейса)
метод checkWithdrawal
- выбрасывает исключение WalletIsBlockedException, если кошелек заблокирован
- выбрасывает исключение InsufficientWalletAmountException, если сумма снятия(amountToWithdraw) больше, чем сумма(amount) в кошельке
метод withdraw
- списывает amountToWithdraw из кошелька
метод checkTransfer
- выбрасывает исключение WalletIsBlockedException, если кошелек заблокирован
- выбрасывает исключение LimitExceededException, если сумма перевода(amountToTransfer) + сумма(amount) в кошельке превышают лимит кошелька (maxAmount)
метод transfer
- добавляет amountToTransfer к сумме(amount) в кошельке

3. Реализуйте класс com.luxoft.dnepr.courses.regular.User, который наследует интерфейс UserInterface.
- добавьте необходимые поля (руководствуясь геттерами из интерфейса)

4. Реализуйте класс com.luxoft.dnepr.courses.regular.Bank, который наследует интерфейс BankInterface.
- добавьте необходимые поля (руководствуясь геттерами из интерфейса)
- карта Map<Long, UserInterface> users заполняется как, User u = new User(...); users.put(u.getId(), u);
- Реализуйте конструктор Bank(String expectedJavaVersion). В случае, если версия java на Вашей машине отличается
от expectedJavaVersion, должна быть выброшена IllegalJavaVersionError. (получение системных свойств см. http://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html)
- реализуйте метод makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount).
Этот метод переводит сумму (amount) с кошелька пользователя с id=fromUserId на кошелек пользователя с id=toUserId
- метод выбрасывает NoUserFoundException, если пользователь с соответствующим id не найден
- если один из кошельков заблокирован, должно быть выброшено исключение TransactionException
с сообщением "User '${user.name}' wallet is blocked". (в данном случае сначала проверяйте кошелек отправителя,
а затем получателя)
- если у отправителя недостаточно средств для совершения платежа должно быть выброшено исключение TransactionException
с сообщением "User '${user.name}' has insufficient funds (${user.wallet.amount} < ${amountToWithdraw})"
- если при осуществлении перевода лимит кошелька получателя будет превышен должно быть выброшено исключение TransactionException
  с сообщением "User '${user.name}' wallet limit exceeded (${user.wallet.amount} + ${amountToWithdraw} > ${user.wallet.maxAmount})"

VERY IMPORTANT
1. Сообщения в TransactionException должны в точности соответствовать приведенным выше шаблонам
2. Суммы(amounts) должны быть отформатированы как 'xxx.xx', например, 125 -> 125.00 ; 1.1 -> 1.10 ; 0.1 -> 0.10; 125.11165 -> 125.11
3. Примеры message в исключениях:
"User '${user.name}' wallet is blocked" -> "User 'Anton' wallet is blocked"
"User '${user.name}' has insufficient funds (${user.wallet.amount} < ${amountToWithdraw})" -> "User 'Anton' has insufficient funds (50.02 < 55.00)"
"User '${user.name}' wallet limit exceeded (${user.wallet.amount} + ${amountToWithdraw} > ${user.wallet.maxAmount})" -> "User '$Anton' wallet limit exceeded (10.00 + 1.00 > 10.99)"