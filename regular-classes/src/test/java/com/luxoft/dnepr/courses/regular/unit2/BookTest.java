package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Assert;
import org.junit.Test;

import java.util.GregorianCalendar;


public class BookTest {

    private ProductFactory productFactory = new ProductFactory();
    private static final double DELTA = 0.000001;

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();


        Assert.assertEquals(cloned.getCode(), book.getCode());
        Assert.assertEquals(cloned.getName(), book.getName());
        Assert.assertEquals(cloned.getPrice(), book.getPrice(), DELTA);
        Assert.assertEquals(cloned.getPublicationDate(), book.getPublicationDate());

        Assert.assertSame(cloned.getCode(), book.getCode());
        Assert.assertSame(cloned.getName(), book.getName());
        Assert.assertNotSame(cloned.getPublicationDate(), book.getPublicationDate());
    }

    @Test
    public void testEquals() throws Exception {
        Book book1 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code2", "SCJP Sun Certified Programmer for Java 6", 250, new GregorianCalendar(2008, 0, 1).getTime());
        Book book3 = productFactory.createBook("code", "Thinking in Java", 220, new GregorianCalendar(2006, 0, 1).getTime());

        Assert.assertTrue(book1.equals(book3));
        Assert.assertTrue(book3.equals(book1));
        Assert.assertFalse(book1.equals(book2));
        Assert.assertFalse(book2.equals(book1));

    }

    @Test
    public void testHashCode() throws Exception {
        Book book1 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code", "Thinking in Java", 220, new GregorianCalendar(2006, 0, 1).getTime());
        Book book3 = productFactory.createBook("code2", "SCJP Sun Certified Programmer for Java 6", 250, new GregorianCalendar(2008, 0, 1).getTime());
        Book book4 = productFactory.createBook("code2", "SCJP Sun Certified Programmer for Java 6", 250, new GregorianCalendar(2008, 0, 1).getTime());

        Assert.assertEquals(book1.hashCode(), book2.hashCode());
        Assert.assertEquals(book3.hashCode(), book4.hashCode());

    }
}
