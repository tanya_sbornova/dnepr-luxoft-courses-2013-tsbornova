package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Assert;
import org.junit.Test;

public class BreadTest {


    private ProductFactory productFactory = new ProductFactory();
    private static final double DELTA = 0.000001;

    @Test
    public void testClone() throws Exception {

        Bread bread = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        Bread breadCloned = (Bread) bread.clone();

        Assert.assertEquals(breadCloned.getCode(), bread.getCode());
        Assert.assertEquals(breadCloned.getName(), bread.getName());
        Assert.assertEquals(breadCloned.getPrice(), bread.getPrice(), DELTA);
        Assert.assertEquals(breadCloned.getWeight(), bread.getWeight(), DELTA);

        Assert.assertSame(breadCloned.getCode(), bread.getCode());
        Assert.assertSame(breadCloned.getName(), bread.getName());


        Bread bread2 = productFactory.createBread(null, null, 10, 1.5);
        Bread bread2Cloned = (Bread) bread2.clone();

        Assert.assertEquals(bread2Cloned.getCode(), bread2.getCode());
        Assert.assertEquals(bread2Cloned.getName(), bread2.getName());
        Assert.assertEquals(bread2Cloned.getPrice(), bread2.getPrice(), DELTA);
        Assert.assertEquals(bread2Cloned.getWeight(), bread2.getWeight(), DELTA);

        Assert.assertSame(bread2Cloned.getCode(), bread2.getCode());
        Assert.assertSame(bread2Cloned.getName(), bread2.getName());

    }

    @Test
    public void testEquals() throws Exception {
        Bread bread1 = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        Bread bread2 = productFactory.createBread("bread1", "White fresh bread", 12, 1.5);
        Bread bread3 = productFactory.createBread("bread3", "Rye  fresh bread", 8, 1.0);

        Assert.assertTrue(bread1.equals(bread2));
        Assert.assertTrue(bread2.equals(bread1));
        Assert.assertFalse(bread1.equals(bread3));
        Assert.assertFalse(bread3.equals(bread1));

        Bread bread4 = productFactory.createBread(null, null, 10, 1.5);
        Bread bread4Cloned = (Bread) bread4.clone();

        Assert.assertTrue(bread4.equals(bread4Cloned));
        Assert.assertTrue(bread4Cloned.equals(bread4));
        Assert.assertFalse(bread1.equals(bread4));
        Assert.assertFalse(bread4.equals(bread1));

    }

    @Test
    public void testHashCode() throws Exception {
        Bread bread1 = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        Bread bread2 = productFactory.createBread("bread1", "White fresh bread", 15, 1.5);

        Assert.assertEquals(bread1.hashCode(), bread2.hashCode());

        Bread bread4 = productFactory.createBread(null, null, 10, 1.5);
        Bread bread4Cloned = (Bread) bread4.clone();

        Assert.assertEquals(bread4.hashCode(), bread4Cloned.hashCode());

    }
}
