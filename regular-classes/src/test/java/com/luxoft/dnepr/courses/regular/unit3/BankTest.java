package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NullValueException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class BankTest {

    private Bank bank;
    private Map<Long, UserInterface> users;
    private UserInterface userAnton;
    private UserInterface userAndrey;
    private UserInterface userOleg;
    private UserInterface userOlya;
    private UserInterface userVasya;
    private UserInterface userKatya;
    private UserInterface userVanya;
    private UserInterface userVova;

    @Before
    public void prepare() throws Exception {

        bank = new Bank("1.7.0_13");
        users = new HashMap<>();
        fillMapUsers();
    }

    private void fillMapUsers() {

        WalletInterface walletAnton = new Wallet(111L, BigDecimal.valueOf(400), WalletStatus.ACTIVE, BigDecimal.valueOf(1400));
        WalletInterface walletAndrey = new Wallet(222L, BigDecimal.valueOf(500), WalletStatus.ACTIVE, BigDecimal.valueOf(1500));
        WalletInterface walletOleg = new Wallet(333L, BigDecimal.valueOf(600), WalletStatus.ACTIVE, BigDecimal.valueOf(700));
        WalletInterface walletOlya = new Wallet(444L, BigDecimal.valueOf(600), WalletStatus.BLOCKED, BigDecimal.valueOf(700));
        WalletInterface walletVasya = new Wallet(555L, BigDecimal.valueOf(600), WalletStatus.ACTIVE, BigDecimal.valueOf(700));
        WalletInterface walletKatya = new Wallet(777L, BigDecimal.valueOf(777.88), WalletStatus.ACTIVE, BigDecimal.valueOf(800));
        WalletInterface walletVanya = new Wallet(888L, BigDecimal.valueOf(777.889), WalletStatus.ACTIVE, BigDecimal.valueOf(880));
        WalletInterface walletVova = new Wallet(null, BigDecimal.valueOf(777.88), WalletStatus.BLOCKED, BigDecimal.valueOf(800));

        userAnton = new User(11111L, "Anton", walletAnton);
        userAndrey = new User(22222L, "Andrey", walletAndrey);
        userOleg = new User(33333L, "Oleg", walletOleg);
        userOlya = new User(44444L, "Olya", walletOlya);
        userVasya = new User(55555L, null, walletVasya);
        userKatya = new User(77777L, "Katya", walletKatya);
        userVanya = new User(88888L, "Vanya", walletVanya);
        userVova = new User(88889L, "Vova", walletVova);


        users.put(userAnton.getId(), userAnton);
        users.put(userAndrey.getId(), userAndrey);
        users.put(userOleg.getId(), userOleg);
        users.put(userOlya.getId(), userOlya);
        users.put(userVasya.getId(), userVasya);
        users.put(userKatya.getId(), userKatya);
        users.put(userVanya.getId(), userVanya);
        users.put(userVova.getId(), userVova);

        bank.setUsers(users);
    }

    @Test
    public void testNormal() {

        try {
            bank.makeMoneyTransaction(userAnton.getId(), userAndrey.getId(), BigDecimal.valueOf(300));
            Assert.assertEquals(BigDecimal.valueOf(100), userAnton.getWallet().getAmount());
            Assert.assertEquals(BigDecimal.valueOf(800), userAndrey.getWallet().getAmount());
        } catch (Exception exception) {
            Assert.fail("There should be no exceptions");
        }
    }

    @Test
    public void testNormal2() {

        try {
            bank.makeMoneyTransaction(userAnton.getId(), userAndrey.getId(), BigDecimal.valueOf(-300));
            Assert.assertEquals(BigDecimal.valueOf(700), userAnton.getWallet().getAmount());
            Assert.assertEquals(BigDecimal.valueOf(200), userAndrey.getWallet().getAmount());
            bank.makeMoneyTransaction(userAnton.getId(), userAndrey.getId(), BigDecimal.valueOf(300));
            Assert.assertEquals(BigDecimal.valueOf(400), userAnton.getWallet().getAmount());
            Assert.assertEquals(BigDecimal.valueOf(500), userAndrey.getWallet().getAmount());
            bank.makeMoneyTransaction(userAnton.getId(), userAnton.getId(), BigDecimal.valueOf(100));
            Assert.assertEquals(BigDecimal.valueOf(400), userAnton.getWallet().getAmount());
        } catch (Exception exception) {
            Assert.fail("There should be no exceptions");
        }
    }

    @Test
    public void testError1() {

        testNormal();
        try {
            bank.makeMoneyTransaction(userAnton.getId(), userAndrey.getId(), BigDecimal.valueOf(300));
            Assert.fail("There should be exception TransactionException");
        } catch (NoUserFoundException exception) {
            Assert.fail("There should be no exception NoUserFoundException");
        } catch (TransactionException exception) {
            assertThat(exception.getMessage(), containsString("User '" + userAnton.getName() + "' has insufficient funds (100.00 < 300.00)"));
        }
    }

    @Test
    public void testError2() {

        try {
            bank.makeMoneyTransaction(userAndrey.getId(), userOleg.getId(), BigDecimal.valueOf(200));
            Assert.fail("There should be exception TransactionException");
        } catch (NoUserFoundException exception) {
            Assert.fail("There should be no exception NoUserFoundException");
        } catch (TransactionException exception) {
            assertThat(exception.getMessage(), containsString("User '" + userOleg.getName() + "' wallet limit exceeded (600.00 + 200.00 > 700.00)"));
        }
    }

    @Test
    public void testError3() {

        try {
            bank.makeMoneyTransaction(userOlya.getId(), userOleg.getId(), BigDecimal.valueOf(200));
            Assert.fail("There should be exception TransactionException");
        } catch (NoUserFoundException exception) {
            Assert.fail("There should be no exception NoUserFoundException");
        } catch (TransactionException exception) {
            assertThat(exception.getMessage(), containsString("User '" + userOlya.getName() + "' wallet is blocked"));
        }
    }

    @Test
    public void testError4() {

        testNormal();
        try {
            bank.makeMoneyTransaction(userAnton.getId(), userOlya.getId(), BigDecimal.valueOf(200));
            Assert.fail("There should be exception TransactionException");
        } catch (NoUserFoundException exception) {
            Assert.fail("There should be no exception NoUserFoundException");
        } catch (TransactionException exception) {
            assertThat(exception.getMessage(), containsString("User '" + userOlya.getName() + "' wallet is blocked"));
        }
    }

    @Test
    public void testError5() {

        testNormal();
        try {
            bank.makeMoneyTransaction(userAnton.getId(), userAndrey.getId(), BigDecimal.valueOf(200));
            Assert.fail("There should be exception TransactionException");
        } catch (NoUserFoundException exception) {
            Assert.fail("There should be no exception NoUserFoundException");
        } catch (TransactionException exception) {
            assertThat(exception.getMessage(), containsString("User '" + userAnton.getName() + "' has insufficient funds (100.00 < 200.00)"));
        }
    }

    @Test
    public void testError6() {

        try {
            bank.makeMoneyTransaction(99999L, userAndrey.getId(), BigDecimal.valueOf(200));
            Assert.fail("There should be exception NoUserFoundException");
        } catch (NoUserFoundException exception) {
            assertThat(exception.getMessage(), containsString("User '99999' not found"));
        } catch (TransactionException exception) {
            Assert.fail("There should be exception NoUserFoundException");
        }
    }

    @Test
    public void testError7() {

        testNormal();
        try {
            bank.makeMoneyTransaction(userAnton.getId(), 99999L, BigDecimal.valueOf(200));
            Assert.fail("There should be exception NoUserFoundException");
        } catch (NoUserFoundException exception) {
            assertThat(exception.getMessage(), containsString("User '99999' not found"));
        } catch (TransactionException exception) {
            Assert.fail("There should be exception NoUserFoundException");
        }
    }

    @Test
    public void testError8() {

        testNormal();
        try {
            bank.makeMoneyTransaction(userVasya.getId(), userAnton.getId(), BigDecimal.valueOf(900.999));
            Assert.fail("There should be exception TransactionException");
        } catch (NoUserFoundException exception) {
            Assert.fail("There should be exception TransactionException");
        } catch (TransactionException exception) {
            assertThat(exception.getMessage(), containsString("User '" + userVasya.getName() + "' has insufficient funds (600.00 < 901.00)"));
        }
    }

    @Test
    public void testError9() {

        testNormal();
        try {
            bank.makeMoneyTransaction(userKatya.getId(), userAnton.getId(), BigDecimal.valueOf(900.999));
            Assert.fail("There should be exception TransactionException");
        } catch (NoUserFoundException exception) {
            Assert.fail("There should be exception TransactionException");
        } catch (TransactionException exception) {
            assertThat(exception.getMessage(), containsString("User '" + userKatya.getName() + "' has insufficient funds (777.88 < 901.00)"));
        }
    }

    @Test
    public void testError11() {

        try {
            new Bank("1.7.0_13");
        } catch (IllegalJavaVersionError exception) {
            Assert.fail("There should be no exceptions");
        }
        try {
            new Bank("1.7.0_12");
            Assert.fail("There should be exceptions");
        } catch (IllegalJavaVersionError exception) {
            assertThat(exception.getMessage(), containsString("Illegal Java version. Expected: 1.7.0_12, actual: 1.7.0_13"));
        }
        try {
            new Bank(null);
            Assert.fail("There should be exceptions");
        } catch (IllegalJavaVersionError exception) {
            assertThat(exception.getMessage(), containsString("Illegal Java version. Expected: null, actual: 1.7.0_13"));
        }
    }

    @Test
    public void testError12() {

        try {
            bank.makeMoneyTransaction(null, userAnton.getId(), BigDecimal.valueOf(900.999));
            Assert.fail("There should be exception NoUserFoundException");
        } catch (NoUserFoundException exception) {
            assertThat(exception.getMessage(), containsString("User 'null' not found"));
        } catch (TransactionException exception) {
            Assert.fail("There should be exception NoUserFoundException");
        }
    }

    @Test
    public void testError13() {

        try {
            bank.makeMoneyTransaction(userAnton.getId(), null, BigDecimal.valueOf(100.999));
            Assert.fail("There should be exception NoUserFoundException");
        } catch (NoUserFoundException exception) {
            assertThat(exception.getMessage(), containsString("User 'null' not found"));
        } catch (TransactionException exception) {
            Assert.fail("There should be exception NoUserFoundException");
        }
    }

    @Test
    public void testError14() {

        try {
            bank.makeMoneyTransaction(userAnton.getId(), null, BigDecimal.valueOf(100.999));
            Assert.fail("There should be exception NoUserFoundException");
        } catch (NoUserFoundException exception) {
            assertThat(exception.getMessage(), containsString("User 'null' not found"));
        } catch (TransactionException exception) {
            Assert.fail("There should be exception NoUserFoundException");
        }
    }

    @Test
    public void testError15() {

        try {
            bank.makeMoneyTransaction(userAnton.getId(), userKatya.getId(), null);
            Assert.fail("There should be exception NullValueException");
        } catch (NullValueException exception) {
            assertThat(exception.getMessage(), containsString("Illegal argument amount in Bank.makeMoneyTransaction()"));
        } catch (NoUserFoundException | TransactionException exception) {
            Assert.fail("There should be exception NullValueException");
        }
    }

    @Test
    public void testError16() {

        try {
            bank.makeMoneyTransaction(userAnton.getId(), userKatya.getId(), null);
            Assert.fail("There should be exception NullValueException");
        } catch (NullValueException exception) {
            assertThat(exception.getMessage(), containsString("Illegal argument amount in Bank.makeMoneyTransaction()"));
        } catch (NoUserFoundException | TransactionException exception) {
            Assert.fail("There should be exception NullValueException");
        }
    }

    @Test
    public void testError17() {

        try {
            bank.makeMoneyTransaction(userVova.getId(), userOleg.getId(), BigDecimal.valueOf(200));
            Assert.fail("There should be exception TransactionException");
        } catch (NoUserFoundException exception) {
            Assert.fail("There should be exception TransactionException");
        } catch (TransactionException exception) {
            assertThat(exception.getMessage(), containsString("User '" + userVova.getName() + "' wallet is blocked"));
        }
    }

    @Test
    public void testError18() {

        userVova.getWallet().setStatus(WalletStatus.ACTIVE);
        try {
            bank.makeMoneyTransaction(userVova.getId(), userAndrey.getId(), BigDecimal.valueOf(200));
        } catch (Exception exception) {
            Assert.fail("There should be no exceptions");
        }
    }

    @Test
    public void testError19() {

        try {
            bank.makeMoneyTransaction(userVanya.getId(), userAndrey.getId(), BigDecimal.valueOf(200));
        } catch (Exception exception) {
            Assert.fail("There should be no exceptions");
        }
    }

    @Test
    public void testNull() {

        users.remove(userVanya.getId());
        userVanya.setId(null);
        users.put(userVanya.getId(), userVanya);
        try {
            bank.makeMoneyTransaction(userVanya.getId(), userAndrey.getId(), BigDecimal.valueOf(200));
            Assert.assertEquals(BigDecimal.valueOf(577.889), userVanya.getWallet().getAmount());
            Assert.assertEquals(BigDecimal.valueOf(700), userAndrey.getWallet().getAmount());
        } catch (Exception exception) {
            Assert.fail("There should be no exceptions");
        }
    }


}

