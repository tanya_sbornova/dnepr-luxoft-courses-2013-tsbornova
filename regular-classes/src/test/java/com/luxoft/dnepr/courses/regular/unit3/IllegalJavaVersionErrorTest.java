package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class IllegalJavaVersionErrorTest {
    String errorMessage = "Illegal Java version.";

    @Test
    public void throwIllegalJavaVersionErrorTest() {

        String expectedJavaVersion = "1.7.0_14";
        String actualJavaVersion = "1.7.0_13";
        errorMessage += " Expected: " + expectedJavaVersion + ", actual: " + actualJavaVersion;

        try {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, errorMessage);

        } catch(IllegalJavaVersionError exception) {
            Assert.assertEquals(errorMessage, exception.getMessage());
            Assert.assertEquals(actualJavaVersion, exception.getActualJavaVersion());
            Assert.assertEquals(expectedJavaVersion, exception.getExpectedJavaVersion());
        }

    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void throwIllegalJavaVersionErrorTest2() {

        exception.expect(IllegalJavaVersionError.class);
        exception.expectMessage("Illegal Java version.");
        throw new IllegalJavaVersionError(null,null,errorMessage);
    }

    @Test(expected = IllegalJavaVersionError.class)
    public void throwIllegalJavaVersionErrorTest3() {

        throw new IllegalJavaVersionError(null,null,null);
    }
}
