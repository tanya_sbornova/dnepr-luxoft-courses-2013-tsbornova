package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class InsufficientWalletAmountExceptionTest {
    @Test
    public void throwInsufficientWalletAmountExceptionTest() {

        Long walletId;
        BigDecimal amountToWithdraw;
        BigDecimal amountInWallet;


        walletId = 123L;
        amountToWithdraw = BigDecimal.valueOf(400);
        amountInWallet =  BigDecimal.valueOf(700);
        String errorMessage = "Insufficient funds";

        try {
            throw new InsufficientWalletAmountException(walletId, amountToWithdraw, amountInWallet, errorMessage);

        } catch (InsufficientWalletAmountException exception) {
            Assert.assertEquals(errorMessage, exception.getMessage());
            Assert.assertEquals(walletId, exception.getWalletId());
            Assert.assertEquals(amountInWallet, exception.getAmountInWallet());
            Assert.assertEquals(amountToWithdraw, exception.getAmountToWithdraw());
        }

        try {
            throw new InsufficientWalletAmountException(null, null, null, errorMessage);

        } catch (InsufficientWalletAmountException exception) {

            Assert.assertEquals(errorMessage, exception.getMessage());
            Assert.assertTrue(exception.getWalletId() == null);
            Assert.assertTrue(exception.getAmountInWallet() == null);
            Assert.assertTrue(exception.getAmountToWithdraw() == null);
        }

    }

}
