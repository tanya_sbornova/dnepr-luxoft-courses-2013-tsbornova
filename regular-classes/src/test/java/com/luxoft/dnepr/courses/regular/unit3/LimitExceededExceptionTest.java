package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class LimitExceededExceptionTest {
    @Test
    public void throwLimitExceededExceptionTest() {

        Long walletId;
        BigDecimal amountToTransfer;
        BigDecimal amountInWallet;

        walletId = 123L;
        amountToTransfer = BigDecimal.valueOf(400);
        amountInWallet =  BigDecimal.valueOf(700);
        String errorMessage = "Wallet limit exceeded ";

        try {
            throw new LimitExceededException(walletId, amountToTransfer, amountInWallet, errorMessage);

        } catch (LimitExceededException exception) {
            Assert.assertEquals(errorMessage, exception.getMessage());
            Assert.assertEquals(walletId, exception.getWalletId());
            Assert.assertEquals(amountInWallet, exception.getAmountInWallet());
            Assert.assertEquals(amountToTransfer, exception.getAmountToTransfer());
        }

        try {
            throw new LimitExceededException(null, null, null, errorMessage);

        } catch (LimitExceededException exception) {

            Assert.assertEquals(errorMessage, exception.getMessage());

        }

    }

}
