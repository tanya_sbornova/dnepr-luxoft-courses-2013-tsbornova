package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import org.junit.Assert;
import org.junit.Test;

public class NoUserFoundExceptionTest {

    @Test
    public void throwNoUserFoundExceptionTest() {

        Long userId = 123456789L;
        String errorMessage = "User " + userId + " not found";

        try {
            throw new NoUserFoundException(userId, errorMessage);

        } catch (NoUserFoundException exception) {
            Assert.assertEquals(errorMessage, exception.getMessage());
            Assert.assertEquals(userId, exception.getUserId());

        }
        userId = 0L;
        try {
            throw new NoUserFoundException(userId, null);

        } catch (NoUserFoundException exception) {

            Assert.assertEquals(null, exception.getMessage());
            Assert.assertEquals(userId, exception.getUserId());

        }

    }


}
