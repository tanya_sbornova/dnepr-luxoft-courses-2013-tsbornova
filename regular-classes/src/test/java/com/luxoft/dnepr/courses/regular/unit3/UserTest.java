package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.NullValueException;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;


public class UserTest {

    @Test
    public void testErrorCreateUser() {

        try {
            new User(55555L, "Vasya", null);
            fail("Should have thrown an NullValueException");
        } catch (NullValueException e) {
            assertThat(e.getMessage(), containsString("Illegal argument wallet in User constructor"));
        }
    }

    @Test
    public void testErrorSetUserWallet() {

        WalletInterface walletVasya = new Wallet(555L, BigDecimal.valueOf(600), WalletStatus.ACTIVE, BigDecimal.valueOf(700));
        try {
            UserInterface userVasya = new User(55555L, "Vasya", walletVasya);
            userVasya.setWallet(null);
            fail("Should have thrown an NullValueException");
        } catch (NullValueException e) {
            assertThat(e.getMessage(), containsString("Illegal argument wallet in User.setWallet()"));
        }
    }
}