package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Assert;
import org.junit.Test;

public class WalletIsBlockedExceptionTest {

    @Test
    public void throwNoUserFoundExceptionTest() {

        Long walletId = 98765L;
        String errorMessage = "Wallet is blocked";

        try {
            throw new WalletIsBlockedException(walletId, errorMessage);

        } catch (WalletIsBlockedException exception) {
            Assert.assertEquals(errorMessage, exception.getMessage());
            Assert.assertEquals(98765, (long) exception.getWalletId());
        }

        walletId = 98765L;
        try {
            throw new WalletIsBlockedException(walletId, null);

        } catch (WalletIsBlockedException exception) {
            Assert.assertEquals(null, exception.getMessage());
            Assert.assertEquals(walletId, exception.getWalletId());
        }


        try {
            throw new WalletIsBlockedException(null, null);

        } catch (WalletIsBlockedException exception) {

            Assert.assertEquals(null, exception.getMessage());
            Assert.assertEquals(null, exception.getWalletId());
        }

    }
}

