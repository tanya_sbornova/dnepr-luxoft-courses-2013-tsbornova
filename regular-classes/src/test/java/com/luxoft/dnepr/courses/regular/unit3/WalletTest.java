package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.NullValueException;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.matchers.JUnitMatchers.containsString;

public class WalletTest {


    @Test
    public void testErrorCreateWallet() {

        try {
            new Wallet(111L, null, WalletStatus.ACTIVE, BigDecimal.valueOf(1400));
            fail("Should have thrown an NullValueException");
        } catch (NullValueException e) {
            assertThat(e.getMessage(), containsString("Illegal argument amount in Wallet constructor"));
        }
    }

    @Test
    public void testErrorCreateWallet2() {

        try {
            new Wallet(111L, BigDecimal.valueOf(1400), WalletStatus.ACTIVE, null);
            fail("Should have thrown an NullValueException");
        } catch (NullValueException e) {
            assertThat(e.getMessage(), containsString("Illegal argument maxAmount in Wallet constructor"));
        }
    }

    @Test
    public void testErrorSetWalletAmount() {

        WalletInterface walletVasya = new Wallet(555L, BigDecimal.valueOf(600), WalletStatus.ACTIVE, BigDecimal.valueOf(700));
        try {
            walletVasya.setAmount(null);
            fail("Should have thrown an NullValueExceptionn");
        } catch (NullValueException e) {
            assertThat(e.getMessage(), containsString("Illegal argument amount in Wallet.setAmount()"));
        }
    }
    @Test
    public void testErrorSetWalletAmmount2() {

        WalletInterface walletVasya = new Wallet(555L, BigDecimal.valueOf(600), WalletStatus.ACTIVE, BigDecimal.valueOf(700));
        try {
            walletVasya.setMaxAmount(null);
            fail("Should have thrown an NullValueException");
        } catch (NullValueException e) {
            assertThat(e.getMessage(), containsString("Illegal argument maxAmount in Wallet.setMaxAmount()"));
        }
    }
}
