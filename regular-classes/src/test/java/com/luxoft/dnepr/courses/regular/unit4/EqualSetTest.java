package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class EqualSetTest {

    private List<String> list;
    private List<Integer> list2;
    private List<Object> list3;
    private Set<String> set;

    @Before
    public void prepare() throws Exception {

        list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list2 = Arrays.asList(new Integer[]{123, 345, -89, 18, 0});
        set = new HashSet<>();
        set.add("1");
        set.add("2");
        set.add("3");
        set.add("4");
        set.add("5");
        set.add("6");
        set.add("7");
        set.add("8");
        list3 = new ArrayList<>();
        list3.add("1");
        list3.add("2");
        list3.add("3");
        list3.add("4");

    }

    @Test
    public void testConstructor() {

        Set<String> equalSet = new EqualSet<>();
        Set<String> hashSet = new HashSet<>();

        Assert.assertEquals(0, equalSet.size());
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
    }

    @Test
    public void testConstructor2() {

        Set<String> equalSet = new EqualSet<>(set);
        Set<String> hashSet = new LinkedHashSet<>(set);
        Assert.assertEquals(8, equalSet.size());
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
    }

    @Test
    public void testConstructor3() {

        list.add("1");
        list.add("2");
        list.add("3");
        Set<String> equalSet = new EqualSet<>(list);
        Set<String> hashSet = new LinkedHashSet<>(list);
        Assert.assertEquals(5, equalSet.size());
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
    }

    @Test
    public void testSize() {

        Set<String> equalSet = new EqualSet<>(list);
        Set<String> hashSet = new LinkedHashSet<>(list);
        Assert.assertEquals(5, equalSet.size());
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
    }

    @Test
    public void testIsEmpty() {

        Set<String> equalSet = new EqualSet<>();
        Set<String> hashSet = new HashSet<>();
        Assert.assertTrue(equalSet.isEmpty());
        Assert.assertTrue(hashSet.isEmpty());
    }

    @Test
    public void testContains() {

        Set<String> equalSet = new EqualSet<>(list);
        Set<String> hashSet = new HashSet<>(list);
        Assert.assertTrue(equalSet.contains("3"));
        Assert.assertTrue(hashSet.contains("3"));
        Assert.assertFalse(equalSet.contains("33"));
        Assert.assertFalse(hashSet.contains("33"));
    }

    @Test
    public void testIterator() {

        Set<String> equalSet = new EqualSet<>(list);
        Set<String> hashSet = new LinkedHashSet<>(list);
        Iterator<String> iteratorEqualSet = equalSet.iterator();
        Iterator<String> iteratorHashSet = hashSet.iterator();
        int i = 0;
        while (iteratorEqualSet.hasNext()) {
            String tempElem = iteratorEqualSet.next();
            Assert.assertEquals(tempElem, list.get(i));
            i++;
        }
        i = 0;
        while (iteratorHashSet.hasNext()) {
            String tempElem = iteratorHashSet.next();
            Assert.assertEquals(tempElem, list.get(i));
            i++;
        }
    }

    @Test
    public void testToArray() {

        Set<Integer> equalSet = new EqualSet<>(list2);
        Set<Integer> hashSet = new LinkedHashSet<>(list2);
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
    }

    @Test
    public void testToArrayT() {

        Set<String> equalSet = new EqualSet<>(list);
        Set<String> hashSet = new LinkedHashSet<>(list);
        String[] strArr = new String[]{"str1", "str2", "str3", "str4", "str5", "str6", "str7", "str8"};
        String[] newArr = equalSet.toArray(strArr);
        String[] etalonArr = new String[]{"1", "2", "3", "4", "5", null, "str7", "str8"};
        Assert.assertArrayEquals(newArr, etalonArr);
        newArr = hashSet.toArray(strArr);
        Assert.assertArrayEquals(newArr, etalonArr);

        strArr = new String[]{"str1", "str2", "str3"};
        newArr = equalSet.toArray(strArr);
        etalonArr = new String[]{"1", "2", "3", "4", "5"};
        Assert.assertArrayEquals(newArr, etalonArr);
        newArr = hashSet.toArray(strArr);
        Assert.assertArrayEquals(newArr, etalonArr);
    }

    @Test
    public void testToArrayT2() {

        Set<String> equalSet = new EqualSet<>(list);
        Set<String> hashSet = new LinkedHashSet<>(list);
        String[] strArr = new String[]{"str1", "str2", "str3", "str4", "str5", "str6", "str7", "str8"};
        String[] newArr = equalSet.toArray(strArr);
        String[] newArr2 = hashSet.toArray(strArr);
        Assert.assertArrayEquals(newArr, newArr2);

        strArr = new String[]{"str1", "str2", "str3"};
        newArr = equalSet.toArray(strArr);
        newArr2 = hashSet.toArray(strArr);
        Assert.assertArrayEquals(newArr, newArr2);
    }

    @Test(expected = NullPointerException.class)
    public void testToArrayTerror1() {

        Set<String> hashSet = new LinkedHashSet<>(list);
        hashSet.toArray(null);
    }

    @Test(expected = NullPointerException.class)
    public void testToArrayTerror2() {

        Set<String> equalSet = new EqualSet<>(list);
        equalSet.toArray(null);
    }

    @Test
    public void testAdd() {

        Set<String> equalSet = new EqualSet<>();
        equalSet.add("7");
        Assert.assertTrue(equalSet.contains("7"));
        Assert.assertFalse(equalSet.add("7"));
    }

    @Test
    public void testAddNull() {

        Set<String> equalSet = new EqualSet<>();
        equalSet.add(null);
        Assert.assertTrue(equalSet.contains(null));
        Assert.assertFalse(equalSet.add(null));
    }

    @Test
    public void testRemove() {

        Set<String> equalSet = new EqualSet<>(list);
        Assert.assertTrue(equalSet.remove("3"));
        Assert.assertFalse(equalSet.remove("22"));
    }

    @Test
    public void testRemoveNull() {

        Set<String> equalSet = new EqualSet<>();
        equalSet.add(null);
        Assert.assertTrue(equalSet.remove(null));
        Assert.assertFalse(equalSet.remove(null));
    }

    @Test
    public void testAddAll() {

        Set<String> equalSet = new EqualSet<>();
        Assert.assertTrue(equalSet.addAll(list));
        Assert.assertFalse(equalSet.addAll(list));

        Set<String> hashSet = new HashSet<>();
        Assert.assertTrue(hashSet.addAll(list));
        Assert.assertFalse(hashSet.addAll(list));
    }

    @Test
    public void testAddAll2() {

        list.add("1");
        list.add("1");
        Set<String> equalSet = new EqualSet<>();
        equalSet.addAll(list);

        Set<String> hashSet = new LinkedHashSet<>();
        hashSet.addAll(list);
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
    }

    @Test
    public void testAddAll3() {

        Set<String> equalSet = new EqualSet<>(list);
        equalSet.addAll(set);

        Set<String> hashSet = new LinkedHashSet<>(list);
        hashSet.addAll(set);
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
    }

    @Test(expected = NullPointerException.class)
    public void testAddAllNull() {

        Set<String> hashSet = new HashSet<>();
        hashSet.containsAll(null);
    }

    @Test(expected = NullPointerException.class)
    public void testAddAllNull2() {

        Set<String> equalSet = new EqualSet<>();
        equalSet.containsAll(null);
    }

    @Test
    public void testContainsAll() {

        list3.add("1");
        list3.add("1");
        Set<String> equalSet = new EqualSet<>(list);
        Assert.assertTrue(equalSet.containsAll(list3));
        Assert.assertFalse(equalSet.containsAll(set));

        Set<String> hashSet = new HashSet<>(list);
        Assert.assertTrue(hashSet.containsAll(list3));
        Assert.assertFalse(hashSet.containsAll(set));
    }

    @Test(expected = NullPointerException.class)
    public void testContainsAllNull() {

        Set<String> hashSet = new HashSet<>(list);
        hashSet.containsAll(null);
    }

    @Test(expected = NullPointerException.class)
    public void testContainsAllNull2() {

        Set<String> equalSet = new EqualSet<>(list);
        equalSet.containsAll(null);
    }

    @Test
    public void testRetainAll() {

        Set<String> equalSet = new EqualSet<>(set);
        Assert.assertTrue(equalSet.retainAll(list));
        Assert.assertFalse(equalSet.retainAll(list));

        Set<String> hashSet = new HashSet<>(set);
        Assert.assertTrue(hashSet.retainAll(list));
        Assert.assertFalse(hashSet.retainAll(list));
    }

    @Test
    public void testRetainAll2() {

        list.add("1");
        list.add("1");
        Set<String> equalSet = new EqualSet<>(set);
        equalSet.retainAll(list);

        Set<String> hashSet = new LinkedHashSet<>(set);
        hashSet.retainAll(list);
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
    }

    @Test
    public void testRetainAll3() {

        Set<String> set1 = new HashSet<>();
        set.add("1");
        set.add("2");
        set.add("3");
        set.add("9");
        set.add("10");

        Set<String> equalSet = new EqualSet<>(set);
        Assert.assertTrue(equalSet.retainAll(set1));
        Assert.assertFalse(equalSet.retainAll(set1));

        Set<String> hashSet = new HashSet<>(set);
        Assert.assertTrue(hashSet.retainAll(set1));
        Assert.assertFalse(hashSet.retainAll(set1));
    }

    @Test
    public void testRetainAll4() {

        Set<String> set1 = new HashSet<>();
        set1.add("1");
        set1.add("2");
        set1.add("3");
        set1.add("9");
        set1.add("10");
        Set<String> set2 = new HashSet<>();
        set2.add("1");
        set2.add("2");
        set2.add("3");
        set2.add("9");
        set2.add("10");
        Set<String> equalSet = new EqualSet<>(set);
        equalSet.retainAll(set1);

        Set<String> hashSet = new HashSet<>(set);
        hashSet.retainAll(set2);
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
        Assert.assertArrayEquals(set1.toArray(), set2.toArray());
    }

    @Test(expected = NullPointerException.class)
    public void testRetainAllNull() {

        Set<String> equalSet = new EqualSet<>(list);
        equalSet.retainAll(null);
    }

    @Test(expected = NullPointerException.class)
    public void testRetainAllNull2() {

        Set<String> hashSet = new HashSet<>(list);
        hashSet.retainAll(null);
    }

    @Test
    public void testRemoveAll() {

        list.add("9");
        list.add("10");
        Set<String> equalSet = new EqualSet<>(set);
        Assert.assertTrue(equalSet.removeAll(list));
        Assert.assertFalse(equalSet.removeAll(list));

        Set<String> hashSet = new HashSet<>(set);
        Assert.assertTrue(hashSet.removeAll(list));
        Assert.assertFalse(hashSet.removeAll(list));
    }

    @Test
    public void testRemoveAll2() {

        Set<String> equalSet = new EqualSet<>(set);
        equalSet.removeAll(list);

        Set<String> hashSet = new HashSet<>(set);
        hashSet.removeAll(list);
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
    }

    @Test
    public void testRemoveAll3() {

        Set<String> equalSet = new EqualSet<>(list);
        equalSet.removeAll(set);

        Set<String> hashSet = new HashSet<>(list);
        hashSet.removeAll(set);
        Assert.assertArrayEquals(equalSet.toArray(), hashSet.toArray());
        Assert.assertEquals(0, equalSet.size());
    }

    @Test(expected = NullPointerException.class)
    public void testRemoveAllNull() {

        Set<String> hashSet = new HashSet<>(set);
        hashSet.removeAll(null);
    }

    @Test(expected = NullPointerException.class)
    public void testRemoveAllNull2() {

        Set<String> equalSet = new EqualSet<>(set);
        equalSet.removeAll(null);
    }

    @Test
    public void testClear() {

        Set<String> equalSet = new EqualSet<>(set);
        equalSet.clear();
        Assert.assertEquals(0, equalSet.size());
        Assert.assertTrue(equalSet.isEmpty());

        Set<String> hashSet = new HashSet<>(set);
        hashSet.clear();
        Assert.assertEquals(0, equalSet.size());
        Assert.assertTrue(equalSet.isEmpty());
    }

    @Test
    public void testClear2() {

        Set<String> equalSet = new EqualSet<>();
        equalSet.clear();
        Assert.assertEquals(0, equalSet.size());
        Assert.assertTrue(equalSet.isEmpty());

        Set<String> hashSet = new HashSet<>();
        hashSet.clear();
        Assert.assertEquals(0, equalSet.size());
        Assert.assertTrue(equalSet.isEmpty());
    }

    @Test
    public void testEquals() {

        Set<String> equalSet = new EqualSet<>(set);
        Set<String> equalSet2 = new EqualSet<>(set);
        Set<String> equalSet3 = new EqualSet<>(list);
        Assert.assertTrue(equalSet.equals(equalSet2));
        Assert.assertTrue(equalSet2.equals(equalSet));
        Assert.assertFalse(equalSet.equals(equalSet3));
        Assert.assertFalse(equalSet3.equals(equalSet));
    }

    @Test
    public void testEquals2() {

        Set<String> equalSet = new EqualSet<>(set);
        Assert.assertFalse(equalSet.equals(null));
    }

    @Test
    public void testHashcode() {

        Set<String> equalSet = new EqualSet<>(set);
        Set<String> equalSet2 = new EqualSet<>(set);
        Assert.assertEquals(equalSet.hashCode(), equalSet2.hashCode());
    }

}