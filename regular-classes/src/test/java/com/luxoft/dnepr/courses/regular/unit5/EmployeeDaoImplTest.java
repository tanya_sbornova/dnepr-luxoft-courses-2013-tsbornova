package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmployeeDaoImplTest {
    private EmployeeDaoImpl employeeDaoImpl;
    private Employee employee1, employee2, employee3, employee4, employee5, employee6, employee7;
    private Redis redis4;

    @Before
    public void prepare() throws Exception {

        employeeDaoImpl = new EmployeeDaoImpl();
        employee1 = new Employee();
        employee1.setSalary(500);
        employee2 = new Employee();
        employee2.setId(111112L);
        employee2.setSalary(600);
        employee3 = new Employee();
        employee3.setSalary(700);
        employee4 = new Employee();
        employee4.setId(222222L);
        employee4.setSalary(800);
        employee5 = new Employee();
        employee5.setSalary(900);
        employee6 = new Employee();
        employee6.setSalary(1000);
        employee7 = new Employee();
        employee7.setSalary(1000);
        redis4 = new Redis();
        redis4.setId(555L);
        redis4.setWeight(80);
    }

    @Test
    public void test1() {

        employeeDaoImpl.save(employee1);
        Assert.assertEquals(Long.valueOf(1), employee1.getId());
        employeeDaoImpl.save(employee2);
        Assert.assertEquals(employee2, employeeDaoImpl.get(111112L));
        employeeDaoImpl.save(employee3);
        Assert.assertEquals(Long.valueOf(111113), employee3.getId());
//        employeeDaoImpl.save(redis4);
        employeeDaoImpl.save(employee4);
        employeeDaoImpl.save(employee5);
        Assert.assertEquals(Long.valueOf(222223), employee5.getId());
        employeeDaoImpl.save(employee6);
        Assert.assertEquals(Long.valueOf(222224), employee6.getId());
//        int sizeStorage = employeeDaoImpl.getEntityStorage().getEntities().size();
//        Assert.assertEquals(6, sizeStorage);

        Assert.assertEquals(employee1, employeeDaoImpl.get(1L));
        Assert.assertEquals(employee2, employeeDaoImpl.get(111112L));
        Assert.assertEquals(employee3, employeeDaoImpl.get(111113L));
        Assert.assertEquals(null, employeeDaoImpl.get(13L));

        Assert.assertTrue(employeeDaoImpl.delete(1L));
        Assert.assertFalse(employeeDaoImpl.delete(1L));
        Assert.assertFalse(employeeDaoImpl.delete(111119L));

        try {
            employeeDaoImpl.save(employee2);
        } catch (UserAlreadyExist exception) {
            Assert.assertEquals(exception.getMessage(), "User 111112 already exist");
        }

        try {
            employeeDaoImpl.update(employee7);
        } catch (UserNotFound exception) {
            Assert.assertEquals(exception.getMessage(), "User null not found");
        }

        employee7.setId(111113L);
        try {
            employeeDaoImpl.save(employee7);
        } catch (UserAlreadyExist exception) {
            Assert.assertEquals(exception.getMessage(), "User 111113 already exist");
        }
        employee7.setId(111114L);
        employeeDaoImpl.save(employee7);
        Assert.assertEquals(employee7,employeeDaoImpl.get(111114L));
    }
}
