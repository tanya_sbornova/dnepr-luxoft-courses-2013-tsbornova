package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Before;
import org.junit.Test;

public class MixTest {
    private EmployeeDaoImpl employeeDaoImpl;
    private RedisDaoImpl redisDaoImpl;
    private Employee employee1, employee2, employee3;
    private Redis redis1, redis2, redis3, redis4, redis5;

    @Before
    public void prepare() throws Exception {

        employeeDaoImpl = new EmployeeDaoImpl();
        employee1 = new Employee();
        employee1.setSalary(500);
        employee2 = new Employee();
        employee2.setId(111112L);
        employee2.setSalary(600);
        employee3 = new Employee();
        employee3.setSalary(700);

        redisDaoImpl = new RedisDaoImpl();
        redis1 = new Redis();
        redis1.setWeight(50);
        redis2 = new Redis();
        redis2.setWeight(60);
        redis3 = new Redis();
        redis3.setId(555L);
        redis3.setWeight(70);
        redis4 = new Redis();
        redis4.setId(555L);
        redis4.setWeight(80);
    }

    @Test
    public void test2() {
    IDao<Employee> dao1 = new EmployeeDaoImpl();
    Employee employee = new Employee();
    employee.setId(1L);
    employee.setSalary(1);
    dao1.save(employee);
    IDao<Employee> dao2 = new EmployeeDaoImpl();
    dao2.get(1L);
    }

    /*@Test
    public void test1() {

        employeeDaoImpl.save(employee1);
        Assert.assertEquals(Long.valueOf(1), employee1.getId());
        employeeDaoImpl.save(employee2);
        Assert.assertEquals(employee2, employeeDaoImpl.get(111112L));
        employeeDaoImpl.save(employee3);
        Assert.assertEquals(Long.valueOf(111113), employee3.getId());
        int sizeStorage = employeeDaoImpl.getEntityStorage().getEntities().size();
        Assert.assertEquals(3, sizeStorage);
        redisDaoImpl.save(redis1);
        Assert.assertEquals(Long.valueOf(1), redis1.getId());
        redisDaoImpl.save(redis2);
        Assert.assertEquals(Long.valueOf(2), redis2.getId());
        Assert.assertEquals(redis2, redisDaoImpl.get(2L));
        redisDaoImpl.save(redis3);
        Assert.assertEquals(Long.valueOf(555), redis3.getId());
        sizeStorage = redisDaoImpl.getEntityStorage().getEntities().size();
        Assert.assertEquals(3, sizeStorage);

        Assert.assertEquals(employee1, employeeDaoImpl.get(1L));
        Assert.assertEquals(employee2, employeeDaoImpl.get(111112L));
        Assert.assertEquals(employee3, employeeDaoImpl.get(111113L));
        Assert.assertEquals(null, employeeDaoImpl.get(13L));

        Assert.assertTrue(employeeDaoImpl.delete(1L));
        Assert.assertFalse(employeeDaoImpl.delete(1L));
        Assert.assertFalse(employeeDaoImpl.delete(111119L));

        try {
            employeeDaoImpl.save(employee2);
        } catch (UserAlreadyExist exception) {
            Assert.assertEquals(exception.getMessage(), "User 111112 already exist");
        }
        Assert.assertEquals(redis1, redisDaoImpl.get(1L));
        Assert.assertEquals(redis2, redisDaoImpl.get(2L));
        Assert.assertEquals(redis3, redisDaoImpl.get(555L));

        redisDaoImpl.update(redis4);
        redis5 = redisDaoImpl.get(555L);
        Assert.assertEquals(80, redis5.getWeight());
    }  */
}
