package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Before;

public class RedisDaoImplTest {
    private RedisDaoImpl redisDaoImpl;
    private Redis redis1, redis2, redis3, redis4;
    private Employee employee1;

    @Before
    public void prepare() throws Exception {

        redisDaoImpl = new RedisDaoImpl();
        redis1 = new Redis();
        redis1.setWeight(50);
        redis2 = new Redis();
        redis2.setWeight(60);
        redis3 = new Redis();
        redis3.setId(555L);
        redis3.setWeight(70);
        redis4 = new Redis();
        redis4.setId(555L);
        redis4.setWeight(80);
        employee1 = new Employee();
        employee1.setSalary(500);
    }

/*    @Test
    public void test1() {
        Redis redis5;

        redisDaoImpl.save(redis1);
        Assert.assertEquals(Long.valueOf(1), redis1.getId());
        redisDaoImpl.save(redis2);
        Assert.assertEquals(Long.valueOf(2), redis2.getId());
        Assert.assertEquals(redis2, redisDaoImpl.get(2L));
        redisDaoImpl.save(redis3);
        Assert.assertEquals(Long.valueOf(555), redis3.getId());

        Assert.assertEquals(null, redisDaoImpl.save(null));

        Assert.assertEquals(redis1, redisDaoImpl.get(1L));
        Assert.assertEquals(redis2, redisDaoImpl.get(2L));
        Assert.assertEquals(redis3, redisDaoImpl.get(555L));

        redisDaoImpl.update(redis4);
        redis5 = redisDaoImpl.get(555L);
        Assert.assertEquals(80, redis5.getWeight());
        Assert.assertEquals(null, redisDaoImpl.update(null));

        Assert.assertTrue(redisDaoImpl.delete(1L));
        Assert.assertFalse(redisDaoImpl.delete(1L));
        Assert.assertFalse(redisDaoImpl.delete(111119L));

        try {
            redisDaoImpl.save(redis2);
        } catch (UserAlreadyExist exception) {
            Assert.assertEquals(exception.getMessage(), "User 2 already exist");
        }

        redis5.setId(777L);
        redis5.setWeight(90);

        try {
            redisDaoImpl.update(redis5);
        } catch (UserNotFound exception) {
            Assert.assertEquals(exception.getMessage(), "User 777 not found");
        }
    }   */
}
