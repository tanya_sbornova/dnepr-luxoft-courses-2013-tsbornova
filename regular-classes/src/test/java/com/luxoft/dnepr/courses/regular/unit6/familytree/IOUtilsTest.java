package com.luxoft.dnepr.courses.regular.unit6.familytree;

import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class IOUtilsTest {
    @Test
    public void simpleTest() throws IOException, ClassNotFoundException {

        Person mark = new PersonImpl("Mark", "English", null, null, Gender.MALE, 32);
        Person sara = new PersonImpl("Sara", "English", null, null, Gender.FEMALE, 32);
        FamilyTree ft = FamilyTreeImpl.create(new PersonImpl(null, "American", mark, sara, Gender.MALE, 1));
        System.out.println(ft.getCreationTime());
        IOUtils.save("D:\\file1.json", ft);

        FamilyTree ftDeserialized = IOUtils.load("D:\\file1.json");

        System.out.println(ft.getCreationTime());
        Assert.assertEquals(ft.getRoot(), ftDeserialized.getRoot());
        Assert.assertEquals(ft.getRoot().getFather(), ftDeserialized.getRoot().getFather());
        Assert.assertEquals(ft.getRoot().getMother(), ftDeserialized.getRoot().getMother());
        Assert.assertEquals(null, ftDeserialized.getRoot().getName());
        Assert.assertEquals("American", ftDeserialized.getRoot().getEthnicity());
        Assert.assertEquals(Gender.MALE, ftDeserialized.getRoot().getGender());
        Assert.assertEquals(1, ftDeserialized.getRoot().getAge());
        Assert.assertEquals("Mark", ftDeserialized.getRoot().getFather().getName());
        Assert.assertEquals("English", ftDeserialized.getRoot().getFather().getEthnicity());
        Assert.assertEquals(null, ftDeserialized.getRoot().getFather().getFather());
        Assert.assertEquals(null, ftDeserialized.getRoot().getFather().getMother());
        Assert.assertEquals(Gender.MALE, ftDeserialized.getRoot().getFather().getGender());
        Assert.assertEquals(32, ftDeserialized.getRoot().getFather().getAge());
        Assert.assertEquals("Sara", ftDeserialized.getRoot().getMother().getName());
        Assert.assertEquals("English", ftDeserialized.getRoot().getMother().getEthnicity());
        Assert.assertEquals(null, ftDeserialized.getRoot().getMother().getFather());
        Assert.assertEquals(null, ftDeserialized.getRoot().getMother().getMother());
        Assert.assertEquals(Gender.FEMALE, ftDeserialized.getRoot().getMother().getGender());
        Assert.assertEquals(32, ftDeserialized.getRoot().getMother().getAge());
    }

    @Test
    public void bigTreeTest() throws IOException, ClassNotFoundException {

        Person vasiliyFather = new PersonImpl("Petr", "Ukrainian", null, null, Gender.MALE, 90);
        Person tanyaMother = new PersonImpl("Anna", "Russian", null, null, Gender.FEMALE, 88);
        Person markFather = new PersonImpl("Ivan", "Russian", null, null, Gender.MALE, 60);
        Person markMother = new PersonImpl("Tanya", "Russian", null, tanyaMother, Gender.FEMALE, 57);
        Person mariyaFather = new PersonImpl("Vasiliy", "Ukrainian", vasiliyFather, null, Gender.MALE, 55);
        Person mariyaMother = new PersonImpl("Galya", "Ukrainian", null, null, Gender.FEMALE, 50);
        Person mark = new PersonImpl("Mark", "Russian", markFather, markMother, Gender.MALE, 32);
        Person mariya = new PersonImpl("Mariya", "Ukrainian", mariyaFather, mariyaMother, Gender.FEMALE, 32);
        FamilyTree ft = FamilyTreeImpl.create(new PersonImpl("Anton Kulik", "Ukrainian", mark, mariya, Gender.MALE, 1));
        IOUtils.save("D:\\file.json", ft);

        FamilyTree ftDeserialized = IOUtils.load("D:\\file.json");
        Assert.assertEquals(ft.getRoot(), ftDeserialized.getRoot());
        Assert.assertEquals(ft.getRoot().getFather(), ftDeserialized.getRoot().getFather());
        Assert.assertEquals(ft.getRoot().getMother(), ftDeserialized.getRoot().getMother());
    }

    @Test
    public void allNullTest() throws IOException, ClassNotFoundException {

        FamilyTree ft = FamilyTreeImpl.create(new PersonImpl(null, null, null, null, null, 0));
        IOUtils.save("D:\\file2.json", ft);

        FamilyTree ftDeserialized = IOUtils.load("D:\\file2.json");
        Assert.assertEquals(ft.getRoot(), ftDeserialized.getRoot());
        Assert.assertEquals(ft.getRoot().getFather(), ftDeserialized.getRoot().getFather());
        Assert.assertEquals(ft.getRoot().getMother(), ftDeserialized.getRoot().getMother());
    }

    @Test
    public void allNullTest2() throws IOException, ClassNotFoundException {

        Person mark = new PersonImpl(null, null, null, null, null, 0);
        Person sara = new PersonImpl(null, null, null, null, null, 0);
        FamilyTree ft = FamilyTreeImpl.create(new PersonImpl(null, null, mark, sara, null, 0));
        IOUtils.save("D:\\file3.json", ft);

        FamilyTree ftDeserialized = IOUtils.load("D:\\file3.json");
        Assert.assertEquals(ft.getRoot(), ftDeserialized.getRoot());
        Assert.assertEquals(ft.getRoot().getFather(), ftDeserialized.getRoot().getFather());
        Assert.assertEquals(ft.getRoot().getMother(), ftDeserialized.getRoot().getMother());
    }

    @Test
    public void rootNullTest() throws IOException, ClassNotFoundException {

        FamilyTree ft = FamilyTreeImpl.create(null);
        IOUtils.save("D:\\file4.json", ft);

        FamilyTree ftDeserialized = IOUtils.load("D:\\file4.json");
        Assert.assertEquals(ft.getRoot(), ftDeserialized.getRoot());
    }

    @Test
    public void streamsTest() throws IOException, ClassNotFoundException {

        PipedOutputStream pos = new PipedOutputStream();
        PipedInputStream pis = new PipedInputStream(pos);

        PersonImpl person = new PersonImpl();
        person.setName("Vasya");
        person.setMother(new PersonImpl());
        FamilyTree ftSave = FamilyTreeImpl.create(person);

        IOUtils.save(pos, ftSave);
        FamilyTree ftLoad = IOUtils.load(pis);
        Assert.assertEquals(ftSave.getRoot(), ftLoad.getRoot());
        Assert.assertEquals(ftSave.getRoot().getFather(), ftLoad.getRoot().getFather());
        Assert.assertEquals(ftSave.getRoot().getMother(), ftLoad.getRoot().getMother());
    }
}
