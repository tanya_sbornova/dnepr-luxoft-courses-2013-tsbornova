package com.luxoft.dnepr.courses.regular.unit6.familytree;

import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.*;

public class IOUtilsTest1 {

    @Test
    public void rootIsNullTest() throws IOException, ClassNotFoundException {
        FamilyTree ft = FamilyTreeImpl.create(null);
        assertNull(ft.getRoot());
        testSaveLoad(ft);
    }

    @Test
    public void personIsEmptyTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        FamilyTree ft = FamilyTreeImpl.create(person);
        assertNotNull(ft.getRoot());
        assertNull(ft.getRoot().getName());
        assertNull(ft.getRoot().getEthnicity());
        assertNull(ft.getRoot().getGender());
        assertNull(ft.getRoot().getFather());
        assertNull(ft.getRoot().getMother());
        assertEquals(0, ft.getRoot().getAge());
        testSaveLoad(ft);
    }

    @Test
    public void personIsFullTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setName("Name Sourname");
        person.setGender(Gender.FEMALE);
        person.setEthnicity("Ukrainian");
        person.setAge(25);
        person.setMother(new PersonImpl());
        person.setFather(new PersonImpl());
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void personOneFieldNameTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setName("Name");
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void personOneFieldAgeTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setAge(5);
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void personOneFieldFatherTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setFather(new PersonImpl());
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void personOneFieldMotherTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setMother(new PersonImpl());
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void personOneFieldGenderTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setGender(Gender.MALE);
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void personOneFieldEthnicityTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setEthnicity("ru");
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void personWithoutOneFieldAgeTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setEthnicity("ethnicity");
        person.setName("name");
        person.setGender(Gender.MALE);
        person.setFather(new PersonImpl());
        person.setMother(new PersonImpl());
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void personWithoutOneFieldMotherTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setEthnicity("ethnicity");
        person.setName("name");
        person.setAge(1);
        person.setGender(Gender.MALE);
        person.setFather(new PersonImpl());
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void personWithFullParentsTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        PersonImpl father = new PersonImpl();
        PersonImpl mother = new PersonImpl();
        person.setAge(20);
        person.setEthnicity("qwer");
        person.setGender(Gender.MALE);
        person.setName("zxcv");
        father.setName("Father");
        father.setAge(50);
        father.setEthnicity("Greek");
        father.setGender(Gender.MALE);
        mother.setName("Mother");
        mother.setEthnicity("Indian");
        mother.setAge(60);
        mother.setGender(Gender.FEMALE);
        person.setFather(father);
        person.setMother(mother);
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void nonAlphabeticalCharactersInStringTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        person.setName("~!@#$%^&*()_+|?><    asfasfasf\r\t   ");
        person.setEthnicity(",,,}{}{},,,,");
        person.setGender(Gender.FEMALE);
        person.setAge(9999);
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    @Test
    public void manyNestedLevelsTest() throws IOException, ClassNotFoundException {
        PersonImpl person = new PersonImpl();
        PersonImpl person1 = new PersonImpl();
        PersonImpl person2 = new PersonImpl();
        PersonImpl person3 = new PersonImpl();
        PersonImpl person4 = new PersonImpl();
        PersonImpl person5 = new PersonImpl();
        PersonImpl person6 = new PersonImpl();
        PersonImpl person7 = new PersonImpl();
        PersonImpl person8 = new PersonImpl();
        PersonImpl person9 = new PersonImpl();
        person.setFather(person1);
        person1.setFather(person2);
        person2.setMother(person3);
        person3.setFather(person4);
        person4.setMother(person5);
        person5.setMother(person6);
        person6.setMother(person7);
        person7.setFather(person8);
        person8.setName("Name");
        person8.setAge(51);
        person8.setEthnicity("eth");
        person8.setGender(Gender.MALE);
        person7.setMother(person9);
        person9.setName("Name");
        testSaveLoad(FamilyTreeImpl.create(person));
    }

    private void testSaveLoad(FamilyTree save) throws IOException, ClassNotFoundException {
        FamilyTree load;
        load = saveLoadStream(save);
        equalsPerson(save.getRoot(), load.getRoot());
        load = saveLoadFile(save);
        equalsPerson(save.getRoot(), load.getRoot());
    }

    private FamilyTree saveLoadStream(FamilyTree familyTree) throws IOException, ClassNotFoundException {
        PipedOutputStream pos = new PipedOutputStream();
        PipedInputStream pis = new PipedInputStream(pos);
        IOUtils.save(pos, familyTree);
        return IOUtils.load(pis);
    }

    private FamilyTree saveLoadFile(FamilyTree familyTree) throws IOException, ClassNotFoundException {
        String fileName = "___testJson___.txt";
        IOUtils.save(fileName, familyTree);
        FamilyTree result = IOUtils.load(fileName);
        Path path = new File(fileName).toPath();
        Files.delete(path);
        return result;
    }

    private void equalsPerson(Person one, Person two) {
        if (one == null) {
            assertNull(two);
        } else {
            assertNotNull(two);
            assertEquals(one.getAge(), two.getAge());
            assertEquals(one.getName(), two.getName());
            assertEquals(one.getEthnicity(), two.getEthnicity());
            assertEquals(one.getGender(), two.getGender());
            if (one.getFather() != null) {
                assertNotNull(two.getFather());
                equalsPerson(one.getFather(), two.getFather());
            } else {
                assertNull(two.getFather());
            }
            if (one.getMother() != null) {
                assertNotNull(two.getMother());
                equalsPerson(one.getMother(), two.getMother());
            } else {
                assertNull(two.getMother());
            }
        }
    }
}
