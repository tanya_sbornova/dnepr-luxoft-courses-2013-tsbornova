package com.luxoft.dnepr.courses.regular.unit7;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class _FileCrawlerTest {

    @Test
    public void testExecute() {
        String rootDirPath = getClass().getResource("dir1").getFile();
        FileCrawler crawler = new FileCrawler(rootDirPath, 10);
        FileCrawlerResults results = crawler.execute();

        assertEquals(2, results.getProcessedFiles().size());

        Map<String, ? extends Number> statistics = results.getWordStatistics();
        assertEquals(19, results.getWordStatistics().size());
        assertEquals(3, statistics.get("съешь").intValue());
        assertEquals(3, statistics.get("ещё").intValue());
        assertEquals(3, statistics.get("этих").intValue());
        assertEquals(3, statistics.get("мягких").intValue());
        assertEquals(3, statistics.get("французских").intValue());
        assertEquals(3, statistics.get("булок").intValue());
        assertEquals(3, statistics.get("да").intValue());
        assertEquals(3, statistics.get("выпей").intValue());
        assertEquals(3, statistics.get("чаю").intValue());
        assertEquals(1, statistics.get("3333").intValue());
        assertEquals(2, statistics.get("три").intValue());
        assertEquals(1, statistics.get("тысячи").intValue());
        assertEquals(1, statistics.get("триста").intValue());
        assertEquals(1, statistics.get("тридцать").intValue());
        assertEquals(3, statistics.get("three").intValue());
        assertEquals(1, statistics.get("thousand").intValue());
        assertEquals(1, statistics.get("hundred").intValue());
        assertEquals(1, statistics.get("and").intValue());
        assertEquals(1, statistics.get("thirty").intValue());
    }

 /*   @Test
    public void testExecute2() {
        String rootDirPath = System.getProperty("user.home");
        long s1 = System.currentTimeMillis();
        FileCrawler crawler = new FileCrawler(rootDirPath, 2);
        FileCrawlerResults results1 = crawler.execute();

        long s2 = System.currentTimeMillis();
        System.out.println((s2 - s1) / 1000);
        crawler = new FileCrawler(rootDirPath, 30);
        FileCrawlerResults results2 = crawler.execute();
        long s3 = System.currentTimeMillis();

        System.out.println((s3 - s2) / 1000);
        assertEquals(results1.getProcessedFiles().size(), results2.getProcessedFiles().size());

        Map<String, ? extends Number> statistic1 = results1.getWordStatistics();
        Map<String, ? extends Number> statistic2 = results2.getWordStatistics();
        assertEquals(results1.getWordStatistics().size(), results2.getWordStatistics().size());
        for (String key : statistic1.keySet()) {
            assertEquals(statistic1.get(key).intValue(), statistic2.get(key).intValue());
        }
    }  */

    @Test
    public void testSave() throws InterruptedException {
        final int numThreads = 350;
        final int numSaves = 200000;
        final WordStorage wordStorage = new WordStorage();

        String rootDirPath = System.getProperty("user.home");
        long s1 = System.currentTimeMillis();
        List<Thread> list = new ArrayList<>();
        for (int i = 0; i < numThreads; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < numSaves; j++) {
                        wordStorage.save(Integer.toString(j));
                    }
                }
            });
            list.add(t);
            t.start();
        }
        for (Thread t : list) {
            t.join();
        }
        long s2 = System.currentTimeMillis();
        System.out.println((s2 - s1) / 1000);
        assertEquals(numSaves, wordStorage.getWordStatistics().size());
        for (Number number : wordStorage.getWordStatistics().values()) {
            assertEquals(numThreads, number.intValue());
        }
    }
}
