package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 05.10.13
 * Time: 23:52
 * To change this template use File | Settings | File Templates.
 */
public class LuxoftUtilsTest {

    @Test
    public void getMonthNameTest() {

        Assert.assertEquals("Январь", LuxoftUtils.getMonthName(1, "ru"));
        Assert.assertEquals("May", LuxoftUtils.getMonthName(5, "en"));
        Assert.assertEquals("Unknown Month", LuxoftUtils.getMonthName(-5, "en"));
        Assert.assertEquals("Неизвестный месяц", LuxoftUtils.getMonthName(15, "ru"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(3, ""));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(13, "hindi"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(3, null));
    }

    @Test
    public void binaryToDecimalTest() {

        Assert.assertEquals("5", LuxoftUtils.binaryToDecimal("0101"));
        Assert.assertEquals("0", LuxoftUtils.binaryToDecimal("0"));
        Assert.assertEquals("789", LuxoftUtils.binaryToDecimal("1100010101"));
        Assert.assertEquals("1789", LuxoftUtils.binaryToDecimal("11011111101"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(""));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("123"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(null));
    }

    @Test
    public void decimalToBinaryTest() {

        Assert.assertEquals("0", LuxoftUtils.decimalToBinary("0"));
        Assert.assertEquals("1100010101", LuxoftUtils.decimalToBinary("789"));
        Assert.assertEquals("11011111101", LuxoftUtils.decimalToBinary("1789"));
        Assert.assertEquals("11111111111111111111111111110100", LuxoftUtils.decimalToBinary("-12"));
        Assert.assertEquals(Integer.toBinaryString(5), LuxoftUtils.decimalToBinary("5"));
        Assert.assertEquals(Integer.toBinaryString(+5678), LuxoftUtils.decimalToBinary("+5678"));
        Assert.assertEquals(Integer.toBinaryString(2147483647), LuxoftUtils.decimalToBinary("2147483647"));
        Assert.assertEquals(Integer.toBinaryString(-2147483648), LuxoftUtils.decimalToBinary("-2147483648"));
        Assert.assertEquals("10000000000000000000000000000001", LuxoftUtils.decimalToBinary("2147483649"));
        Assert.assertEquals("10111011110110001000111100111110101", LuxoftUtils.decimalToBinary("-9147483659"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("+-45"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("45f"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(""));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(null));
        Assert.assertEquals("101", LuxoftUtils.decimalToBinary(LuxoftUtils.binaryToDecimal("101")));
        Assert.assertEquals("345", (LuxoftUtils.binaryToDecimal(LuxoftUtils.decimalToBinary("345"))));
        Assert.assertEquals("9147483659", (LuxoftUtils.binaryToDecimal(LuxoftUtils.decimalToBinary("9147483659"))));
        Assert.assertEquals("9223372036854775807", (LuxoftUtils.binaryToDecimal(LuxoftUtils.decimalToBinary("9223372036854775807"))));
        Assert.assertEquals("9223372036854775808", (LuxoftUtils.binaryToDecimal(LuxoftUtils.decimalToBinary("9223372036854775808"))));
    }

    @Test
    public void sortArrayTest() {

        Assert.assertArrayEquals(new int[]{1, 3, 3, 5, 8, 10, 11, 12, 17, 20}, LuxoftUtils.sortArray(new int[]{5, 20, 3, 11, 1, 17, 3, 12, 8, 10}, true));
        Assert.assertArrayEquals(new int[]{1, 3, 3, 5, 8, 10, 11, 12, 17, 20}, LuxoftUtils.sortArray(new int[]{5, 20, 3, 11, 1, 17, 3, 12, 8, 10}, true));
        Assert.assertArrayEquals(new int[]{20, 17, 12, 11, 10, 8, 5, 3, 3, 1}, LuxoftUtils.sortArray(new int[]{5, 20, 3, 11, 1, 17, 3, 12, 8, 10}, false));
        Assert.assertArrayEquals(new int[]{33, 11, 8, 3, 1, 1, 0, -11, -12, -20}, LuxoftUtils.sortArray(new int[]{-11, -20, 33, 11, 1, 1, 3, -12, 8, 0}, false));
        Assert.assertArrayEquals(new int[]{-20, -12, -11, 0, 1, 1, 3, 8, 11, 33}, LuxoftUtils.sortArray(new int[]{-11, -20, 33, 11, 1, 1, 3, -12, 8, 0}, true));
        Assert.assertArrayEquals(new int[]{-100}, LuxoftUtils.sortArray(new int[]{-100}, true));
        Assert.assertArrayEquals(null, LuxoftUtils.sortArray(null, true));
        Assert.assertArrayEquals(new int[]{}, LuxoftUtils.sortArray(new int[]{}, false));
    }

}



