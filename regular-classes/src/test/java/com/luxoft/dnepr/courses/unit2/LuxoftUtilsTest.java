package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 1
 * Date: 12.10.13
 * Time: 14:01
 * To change this template use File | Settings | File Templates.
 */
public class LuxoftUtilsTest {
    private static final double DELTA = 0.000001;

    @Test
    public void testSortArray() {

        Assert.assertArrayEquals(new String[]{"ABCD"}, LuxoftUtils.sortArray(new String[]{"ABCD"}, true));
        Assert.assertArrayEquals(new String[]{"ABCDE", "ABCDF"}, LuxoftUtils.sortArray(new String[]{"ABCDE", "ABCDF"}, true));
        Assert.assertArrayEquals(new String[]{"ABCDF", "ABCDE"}, LuxoftUtils.sortArray(new String[]{"ABCDE", "ABCDF"}, false));
        Assert.assertArrayEquals(new String[]{"zabc", "Zabc", "BCD"}, LuxoftUtils.sortArray(new String[]{"BCD", "zabc", "Zabc"}, false));
        Assert.assertArrayEquals(new String[]{"BCD", "Zabc", "zabc"}, LuxoftUtils.sortArray(new String[]{"BCD", "zabc", "Zabc"}, true));
        Assert.assertArrayEquals(new String[]{"e", "d", "c", "b", "a", "Z", "A"}, LuxoftUtils.sortArray(new String[]{"b", "d", "c", "a", "e", "Z", "A"}, false));
        Assert.assertArrayEquals(new String[]{"A", "Z", "a", "b", "c", "d", "e"}, LuxoftUtils.sortArray(new String[]{"b", "d", "c", "a", "e", "Z", "A"}, true));
        Assert.assertArrayEquals(new String[]{"Keane", "Professor", "can", "cause", "explains", "floods", "hurricanes", "why"}, LuxoftUtils.sortArray(new String[]{"Professor", "Keane", "explains", "why", "hurricanes", "can", "cause", "floods"}, true));
        Assert.assertArrayEquals(new String[]{"why", "hurricanes", "floods",  "explains", "cause", "can", "Professor", "Keane"}, LuxoftUtils.sortArray(new String[]{"Professor", "Keane", "explains", "why", "hurricanes", "can", "cause", "floods"}, false));
        Assert.assertArrayEquals(new String[]{"why"}, LuxoftUtils.sortArray(new String[]{"why"}, false));
        Assert.assertArrayEquals(new String[]{"swim", "can"}, LuxoftUtils.sortArray(new String[]{"can", "swim"}, false));
        Assert.assertArrayEquals(new String[]{"can", "swim",}, LuxoftUtils.sortArray(new String[]{"can", "swim"}, true));
        Assert.assertArrayEquals(new String[]{}, LuxoftUtils.sortArray(new String[]{}, true));
    }

    @Test
    public void testWordAverageLength() {

        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat"), DELTA);
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength(" I  have  a  cat "), DELTA);
        Assert.assertEquals(3.6, LuxoftUtils.wordAverageLength(" We can grow some crops "), DELTA);
        Assert.assertEquals(3.571, LuxoftUtils.wordAverageLength("It has not rained for six monts"), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("cat"), DELTA);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(""), DELTA);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength("  "), DELTA);
    }

    @Test
    public void testReverseWords() {
        Assert.assertEquals("I evah a tac", LuxoftUtils.reverseWords("I have a cat"));
        Assert.assertEquals("abc frt grgr ", LuxoftUtils.reverseWords("cba trf rgrg "));
        Assert.assertEquals("  cba trf rgrg ", LuxoftUtils.reverseWords("  abc frt grgr "));
        Assert.assertEquals("", LuxoftUtils.reverseWords(""));
    }

    @Test
    public void testGetCharEntries() throws Exception {
        Assert.assertArrayEquals(new char[]{'a', 'I', 'c', 'e', 'h', 't', 'v'}, LuxoftUtils.getCharEntries("I have a cat"));
        Assert.assertArrayEquals(new char[]{'A', 'S', 'a', 'z'}, LuxoftUtils.getCharEntries("aaaAAA zzzSSS"));
        Assert.assertArrayEquals(new char[]{'V', 'A', 'a', 'B', 'S', 'b', 'z', 'p'}, LuxoftUtils.getCharEntries("aaabb AAABB VVVVVVV zzpSS"));
        Assert.assertArrayEquals(new char[]{'e', 'l', 'i', 'n', 'h', 'I', 'a', 'g', 'o', 'p', 'r', 's', 't', 'v', 'w', 'y'}, LuxoftUtils.getCharEntries("I really will phone this evening"));
        Assert.assertArrayEquals(new char[]{'d'}, LuxoftUtils.getCharEntries("ddd"));
        Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries(""));
    }

    @Test
    public void testCalculateOverallArea() {

        List<Figure> temp1 = new ArrayList<Figure>();
        temp1.add(new Circle(0.0));
        temp1.add(new Square(0.0));
        temp1.add(new Hexagon(0.0));
        double temp2 = LuxoftUtils.calculateOverallArea(new ArrayList<Figure>(Arrays.asList(new Square(1.5), new Square(8), new Square(1), new Circle(6.0), new Hexagon(1.0))));
        double temp3 = LuxoftUtils.calculateOverallArea(new ArrayList<Figure>(Arrays.asList(new Circle(0.5), new Circle(3.2), new Circle(1.7))));
        List<Figure> temp4 = new ArrayList<Figure>();
        temp4.add(new Circle(0.5));
        List<Figure> temp5 = new ArrayList<Figure>();
        temp5.add(new Hexagon(3.7));
        temp5.add(new Hexagon(5.0));
        List<Figure> figures = new ArrayList<Figure>();
        figures.add(new Square(1));
        figures.add(new Circle(1));


        Assert.assertEquals(0.0, LuxoftUtils.calculateOverallArea(temp1), DELTA);
        Assert.assertEquals(182.945411, temp2, DELTA);
        Assert.assertEquals(42.0345097, temp3, DELTA);
        Assert.assertEquals(0.785398, LuxoftUtils.calculateOverallArea(temp4), DELTA);
        Assert.assertEquals(100.519568, LuxoftUtils.calculateOverallArea(temp5), DELTA);
        Assert. assertEquals(4.14159265, LuxoftUtils.calculateOverallArea(figures), 0.000001);

    }
}
