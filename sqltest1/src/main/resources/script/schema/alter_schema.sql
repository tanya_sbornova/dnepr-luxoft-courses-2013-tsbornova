CREATE TABLE IF NOT EXISTS makers (
    maker_id INT AUTO_INCREMENT,
    maker_name VARCHAR(50)  NOT NULL,
    maker_adress VARCHAR(200),
    CONSTRAINT pk_makers PRIMARY KEY(maker_id)
);

INSERT INTO makers(maker_name, maker_adress)
  VALUES('A', 'AdressA'),('B', 'AdressB'),('C', 'AdressC'),('D', 'AdressD'),('E', 'AdressE');
 
ALTER TABLE product
ADD maker_id INT;

UPDATE product p SET maker_id = 
(SELECT maker_id FROM makers m WHERE p.maker = m.maker_name) WHERE p.maker_id IS NULL;

ALTER TABLE product
MODIFY maker_id INT NOT NULL;

ALTER TABLE product
DROP COLUMN maker;

ALTER TABLE product 
ADD CONSTRAINT fk_product_makers FOREIGN KEY (maker_id) REFERENCES makers(maker_id);

CREATE TABLE IF NOT EXISTS printer_type (
    type_id INT,
    type_name VARCHAR(50)  NOT NULL,
    CONSTRAINT pk_printer_type PRIMARY KEY(type_id)
);

INSERT INTO printer_type(type_id, type_name) VALUES (1,  'Laser');
INSERT INTO printer_type(type_id, type_name) VALUES (2,  'Jet');
INSERT INTO printer_type(type_id, type_name) VALUES (3,  'Matrix');

ALTER TABLE printer
ADD type_id INT;

ALTER TABLE printer
MODIFY color CHAR(1)  DEFAULT 'y'  NOT NULL;

UPDATE printer p SET type_id = 
(SELECT type_id FROM printer_type t WHERE p.type = t.type_name) WHERE p.type_id IS NULL;

ALTER TABLE printer 
ADD CONSTRAINT fk_printer_type FOREIGN KEY (type_id) REFERENCES printer_type(type_id);

ALTER TABLE printer
MODIFY COLUMN type_id INT NOT NULL;

ALTER TABLE printer DROP COLUMN type;

CREATE INDEX ind_pc_price ON pc (price ASC) USING BTREE;
CREATE INDEX ind_laptop_price ON laptop (price ASC) USING BTREE;
CREATE INDEX ind_printer_price ON printer (price ASC) USING BTREE;


