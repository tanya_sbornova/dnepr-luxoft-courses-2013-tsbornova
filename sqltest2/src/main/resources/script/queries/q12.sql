SELECT hd  FROM pc
GROUP BY hd
HAVING count(model) > 1
ORDER BY hd DESC;