SELECT maker_name, price FROM
(SELECT model, price FROM printer 
WHERE color = 'y' AND price IN (SELECT MIN(price) FROM printer WHERE color = 'y')) p,
makers m, product pr WHERE p.model = pr.model AND m.maker_id = pr.maker_id;
