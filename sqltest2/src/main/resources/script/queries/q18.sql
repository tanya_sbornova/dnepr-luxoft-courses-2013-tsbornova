select model from (select price, model from testdb.pc union
select price, model from testdb.laptop union select price, model from testdb.printer) b where price in
(select max(price) from 
(select price from testdb.pc union select price from testdb.laptop union 
select price from testdb.printer) a);