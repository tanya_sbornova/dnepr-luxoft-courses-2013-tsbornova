SELECT maker_id, AVG(hd) avg_hd FROM testdb.pc, testdb.product pr
WHERE pr.model = pc.model and pr.maker_id IN
(SELECT maker_id FROM testdb.product WHERE type = 'Printer')
GROUP BY maker_id;
