SELECT DISTINCT m.maker_name 
FROM makers m, printer pr, product p
WHERE pr.model = p.model and p.maker_id = m.maker_id
ORDER BY maker_name DESC;

