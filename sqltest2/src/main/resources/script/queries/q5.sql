SELECT DISTINCT pr.model, price  
FROM product pr, pc 
WHERE pc.model = pr.model and pr.maker_id = (select maker_id from makers where maker_name = 'B')
UNION
(SELECT DISTINCT pr.model, price  
FROM product pr, laptop lp
WHERE lp.model = pr.model and pr.maker_id = (select maker_id from makers where maker_name = 'B'))
UNION
(SELECT DISTINCT pr.model, price  
FROM product pr, printer p
WHERE p.model = pr.model and pr.maker_id = (select maker_id from makers where maker_name = 'B'));