SELECT DISTINCT m.maker_name 
FROM makers m, pc, product p
WHERE pc.speed >= 450 and pc.model = p.model and p.maker_id = m.maker_id;